Team Type Optimizer
===================

This project attempts to optimize a Pokemon team, type wise.

## History

I never was a hard-core fan of Pokemon. But every now and then I find interesting problems in this francise that pose nice optimization questions.

## Structure

This project consists of several sub-projects that all offer different approaches.

``core`` is a selection of core definitions and functions that contain all basic concepts of types, type combinations, type matrices, etc. For example, this project contains the type matrices of all available generations so far.

``game`` is targeted towards building a team that can tackle a whole game with diverse situations.

``challenge`` is targeted at finding a team for a certain limited situation, usually things like a rival fight, a gym or a regional league.

## Set up

This project is built with sbt and coded in Scala 3. Except for the core sub-project, all sub-projects have launchable applications (mostly in form of tests) that solve one problem or another.