Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / scalaVersion := "3.2.2"
ThisBuild / organization := "de.d_albrecht"

lazy val commonSettings = Seq(
	libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.15",
	libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.15" % Test,
	libraryDependencies += "org.scalatestplus" %% "junit-4-13" % "3.2.15.0" % "test",
	libraryDependencies += "junit" % "junit" % "4.13.2" % Test,
)

lazy val core = (project in file("core"))
	.settings(commonSettings)

lazy val game = (project in file("game"))
	.settings(commonSettings)
	.dependsOn(core)
	.dependsOn(core % "test->test")

lazy val challenge = (project in file("challenge"))
	.settings(commonSettings)
	.dependsOn(core)
	.dependsOn(core % "test->test")
