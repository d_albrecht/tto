Challenge Team Type Optimizer
=============================

The objective of this project is to find a good (if not the perfect) team (type combination wise) to beat a specific challenge, like an evil team hideout, a rival fight, a gym, or a regional league. But the concept of a "challenge" can be defined much broader to whatever use case you have. Still, the same general principles of usability apply as in the ``game``-mode, that there is more to a team than types alone.

## Motivation

On the one hand the ``game``-mode is sort of a "run once, and forget" type of project. The outcome won't ever change unless you come up with new policies (or something might change with the type matrix, which - at this point - is rather unlikely). We found a perfect team and that's that. On the other hand the results of this other project isn't really useful. At least not for any particular game you wanted to (re-)play. But the overall concept ought to be translatable to actual game play, or not? That was the starting question for this spin on the existing ``game``-mode.

## Objective

Where the former ``game``-mode aimed to generate one team for any occasion, that's hardly a situation you would find yourself in really. In reality you have a large range of potential team members and you are facing a limited set of "threats" at any give time. Where the other project neglected almost all aspects other than typing, in reality you are confronted with much more. Where you could beat the whole game with one team, you might not want to, or perhaps want to tackle the game in separate chapters even. Where a universal team would require non-existing type combinations, actual games are limited to a strict sub-set.

Hence, the goal for this optimizer is to find a good (or ideal) team (perhaps even one superior to the ``game``-mode output) in a real game situation that helps you beat a specific chapter of the game. This concept can be broaden further to generate the perfect team for a whole game under realistic circumstances (in comparison to the ``game``-mode, where there is no realistic situation), but that's not the core situation this optimizer is written (and optimized) for. To accomplish this, this optimizer will handle abilities to the best of its capabilities and can take (fixed and abstracted) move sets into account.

## Components

This mode behaves similarly to the ``game``-mode when it comes to the rough approach it takes, while internally it uses a different implementation (which isn't the point of this document, so check the actual implementation for that). This project as well has opponents to take into account and team candidates to choose from. And then there are criteria to which candidates to select for different opponents. This last part is - at least when it comes to pre-made implementations - the main difference to ``game``-mode, as the premise of the team is different. But also the other parts are adapted specifically to this project's theme. For example, in this project we don't focus on what were Member Policies as much as we did back then. On the one hand this might not be necessary because in this mode the user has a more direct influence on what candidates will be considered, on the other hand the matchup of opponents to team members is different (usually stricter) and not having explicit Member Policies leave more room for the Optimizer to build (smaller) teams. Whether a candidate can have some individual drawbacks has to be part of the criteria-implementation that one run uses. In general all candidates are equally valid.

### Opponents

The provided opponents is the core aspect that makes this mode more specific to actual games. It's a list of Battlers (as defined my the ``core`` project). Each Battler can have type effectiveness altering abilities applied to them and can come with a list of attack types it supports. For the scope of this project, attacks are only represented by their type and the list of attacks that a Battler can have represents the exact attacks one opponent can use, in terms of its four attacks. The Optimizer isn't able to interpret move learn sets (all leveling moves, all machine moves, all egg moves), but expects a fixed move set. In this move set, ``Status`` moves aren't included because the type matrix doesn't apply to them. Furthermore, multiple attacks of the same type can be represented by a single type entry in the attack list.

### Candidates

What used to be a Member Policy in the ``game``-mode and worked by excluding type combinations by their properties, in this project you usually explicitly list the candidates that one run could include. In that way, you can also (somewhat) sort out candidates with specific drawbacks at this stage. Similarly to opponents, you are able to apply abilities at this point or specifiy the list of moves that one candidate would use if included in the team. Again, the optimizer isn't able to figure out a good move set based on the complete move learn set, but you can provide variations of the same candidate with varying move sets.

Both the opponents as well as the candidates can be specified in code (or based on the type matrix's combination generator methods) or imported in terms of the TypeCombinationParser. See there for a full definition of the file format to use.

### Matchup Selector

For this mode the general approach is to match some candidate to each opponent. Therefore, it is not really possible to ensure a specific team property. In return, you are able to respond to some more problematic opponent in a specific way. For example, in ``game``-mode it wasn't possible to handle gen 1 because of the (``Water``, ``Dragon``) type combination and the exact way the StabAdvantage policy was implemented. In this mode, you will have to solve this problem somehow if this combnation is part of the opponents list, but you can freely choose which of the candidates will match up the best (even if "the best" isn't ideal either).

This project comes with several pre-made implementations to choose from, but you are free to add your own. You can always programmatically filter any of the two lists you work with, but this project also comes with "plug-in" policies that mimic the Member Policies of the ``game``-mode. Furthermore, it contains policies to:
- ensure a type advantage both offensively and defensively,
- ensure at least neutral damage done and at most neutral damage taken, or
- select the best candidates by either damage done, damage taken, a balance of both, or the overall degree of (type) advantage over the opponent.

## Formatter

This project comes with further utilities that should help apply any team to some specific use case. The core idea is to list all matchups that some optimizer intended you to use.

Say, you have a team optimized in ``game``-mode and you want to know when to use which one, then use this team as the input team with some set of opponents (all unique combinations, or the avaliable combinations from one edition) and let the formatter output which opponent should be faced with which team member. Do you have a team (either self-constructed or from one of the optimizers) and want to know what the biggest thread is for each team member? Use the opponent-perspective, use your team as the ``opponents`` and some set of potential threads as team and the formatter will tell you the most devastating that each team member can be up to (in this context ``team`` and ``opponent`` is reversed in their meaning, but that is due to ``team`` being the side that wants to play optimally, and ``opponent`` is the side that just happens to send in some of their team members). Or, are you in ``challenge``-mode and the optimizer has determined some team, but you are unsure if some team members might be required for too many fights and hence might run low on HP/PP? Use the team-perspective with your team and the opponents and the formatter will show you how often one team member would be used ideally.

This formatter will work with any of the matchup policies. That means, for ``challenge``-mode you can use the exact same policy both times and know exactly what the optimizer was considering. Still, you can always check any optimized team with any other policy. For ``game``-mode you would need a policy that as close as possible resembles your used team policies (member policies won't make a difference here anymore, as candidates that didn't fulfil all member policies weren't selected in either way). Still, you can just use one of the premade matchup policies (or try different ones) and learn several aspects of your team this way. Perhaps, some matchup policy will suggest to use a different team member than the team policy did, but that's fine. Ultimately it's your call for when to use which team member.

Currently, the formatters are written to target std-out, but this could easily be turned into an implementation that would make the code reusable for any programmatical use case.

## ``challenge``-mode vs ``game``-mode

In their current form, the ``challenge``-mode is by far the fastest of the two modes. Even if you abuse it to mimic the ``game``-mode. While one run of the ``game``-mode takes anywhere from seconds to minutes given the used criteria / the size of the search space, running every current test case in the ``challenge``-project takes less than a second to finish (assuming some fixed hardware, your run times might differ, but the ratio should be similar).

But the ``challenge``-mode comes at a cost. This optimization completely neglects any overall team properties. While both modes don't really like to be constrained when it comes to team sizes they generate, the ``challenge``-mode also is only focused on the question how to counter any given opponent. Whether the whole team ends up using only candidates with four times weaknesses, whether you can't resist some specific type at all, whether your team could easily be beaten by some random encounter is completely out of scope.

In contrast the ``game``-mode is generally more a all-rounder. It both takes your team members' strengths and weaknesses into account. And when it encounters a weakness, it ensures that your team has an option to completely turn the tides and swap in a team member that excels in the situation the other team member wasn't built for.

That is not to say that the ``game``-mode couldn't be optimized at this stage because I'm sure it could. The ``challenge``-mode has shown that if you are able to only evaluate matchups between opponents and candidates once and then rely on logical simplification, the optimization is a highly performant task. The same isn't easily translatable to the ``game``-mode, though.

## Testcases

To demonstrate the usage I added an example usa case in ``src/test/scala/de/d_albrecht/tto/resources/RubySapphireLeagueChallenge.scala``. An explanation is linked in the mentioned test case file.
