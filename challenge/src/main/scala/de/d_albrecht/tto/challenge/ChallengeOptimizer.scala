/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package challenge

import Definitions._

import policies._

/**
  * Solves challenges by constructing the smallest possible teams such that for each opponent there has to be at least one team member that is a valid counter according to the given MatchupPolicy.
  * 
  * Implementation Detail: Most of the methods aren't specific to the NamedBattler type but use a generic type.
  * That is both to increase the testability and because the actual type is more an implementation detail than anything specific to the optimizer.
  * But this should not affect the correctness based on the contract of the NamedBattler and the MatchupPolicy.
  * That is, because all names in one of the two sets must be unique and the matchum policy isn't supposed to alter existing or add new battlers.
  * In this regard, even if the generic methods rely on the default implementations of things like equality,
  * if two elements have the same name, the remaining fields must by design be identical, too.
  * 
  * The approach to solve this optimization problem is linked to the boolean satisfiability problem of formulas in conjunctive normal form,
  * where each clause is restricted to only contain positive (non-negated) literals. And the desired solutions have to have the least literals assigned to TRUE.
  */
object ChallengeOptimizer {
	def generateClauses(opponents: List[NamedBattler], candidates: List[NamedBattler], policy: MatchupPolicy): List[List[NamedBattler]] = {
		opponents.map(policy.suggest(_, candidates))
	}

	/**
	  * Inspects the given clauses and extracts those that contain exactly one element. These elements will be returned,
	  * alongside all clauses that still remain, when dropping any that are now fulfilled given the extracted singular elements.
	  * @param clauses The clauses to process
	  * @return The list of remaining clauses and the extracted singular elements
	  */
	def extractSingularClauses[T](clauses: List[List[T]]): (List[List[T]], List[T]) = {
		val singulars = clauses.flatMap{
			case Nil => throw new IllegalStateException("Some clause was empty! Not solvable!")
			case singular :: Nil => Some(singular)
			case _ => None
		}
		(clauses.filter(_.intersect(singulars).isEmpty), singulars)
	}

	/**
	  * Tries to reduce the number of clauses by detecting and droping clauses that are already covered by others.
	  * To do so, this method uses two attempts, first it discards clauses that are exact duplicates.
	  * Second, it will drop clauses that are strict super-sets of other clauses. These super-sets can't add any value to the solution.
	  * Super-sets might influence which team member you select for an opponent, but it won't change the team composition.
	  * @param clauses The clauses to process
	  * @return The list of clauses that have to be fulfilled to satisfy the given clauses
	  */
	def reduceClauses[T](clauses: List[List[T]]): List[List[T]] = {
		val decloned = clauses.distinctBy(_.toSet)
		decloned.filterNot(weak => decloned.exists(strong => strong.size < weak.size && strong.diff(weak).isEmpty))
	}

	/**
	  * Tries to reduce the number of candidates that are used in the clauses.
	  * To do so, this method discards candidates that are exact duplicates with other candidates, aka feature in the same clauses.
	  * To later recover this information, this method also provides mappings from remaining candidates to dropped candidates alongside the altered clauses.
	  * @param clauses The clauses to process
	  * @return The slimmed list of clauses and the mappings to recover the duplicate candidates, should they be part of a solution
	  */
	def reduceCandidates[T](clauses: List[List[T]]): (List[List[T]], List[(T, T)]) = {
		val indexedClauses = clauses.zipWithIndex
		val duplicates = clauses.flatten.distinct.groupBy(c => indexedClauses.filter(_._1.contains(c)).map(_._2).toSet).map(_._2).collect{
			case singular :: (rest@(_ :: _)) => rest.map((singular, _))
		}.flatten.toList
		val toBeRemoved = duplicates.map(_._2)
		(clauses.map(_.diff(toBeRemoved)), duplicates)
	}

	/**
	  * Takes two lists of mappings of candidates and merges the two lists.
	  * In this process there is a clear contextual split between old mappings and new mappings,
	  * as new mappings can potentially re-map already mapped candidates.
	  * Therefore, all new mappings are checked whether old mappings have to be updated and then both lists are merged and returned.
	  * @param oldDups The existing mappings from remaining candidates to dropped candidates
	  * @param newDups The new mappings from remaining candidates to dropped candidates
	  * @return The merged mappings from remaining candidates to dropped candidates
	  */
	def mergeCandidateDuplicates[T](oldDups: List[(T, T)], newDups: List[(T, T)]): List[(T, T)] = {
		oldDups.map(old => newDups.filter(_._2 == old._1) match {
			case Nil => old
			case newDup :: Nil => (newDup._1, old._2)
			case _ => throw new IllegalArgumentException("newDups can't contain several mappings with the same second element")
		}) ::: newDups
	}

	/**
	  * Applies the before defined methods in succession to do one round of simplicifation.
	  * @param clauses The clauses to satify
	  * @return The remaining (not yet satisfied) clauses, the list of necessary candidates to a minimal solution, and the information for duplicates
	  */
	def round[T](clauses: List[List[T]]): (List[List[T]], List[T], List[(T, T)]) = {
		val (remaining, singulars) = extractSingularClauses(reduceClauses(clauses))
		val (slimmed, mappings) = reduceCandidates(remaining)
		(slimmed, singulars, mappings)
	}

	/**
	  * Loops the simplification process until there is no more simplification possible.
	  * This can leave still unsatisfies clauses that need to be solved in a different manner.
	  * @param clauses The clauses to satisfy
	  * @param singulars The already found necessary candidates
	  * @param mappings The already found duplicates
	  * @return The remaining (not yet satisfied) clauses, the list of necessary candidates to a minimal solution, and the information for duplicates
	  */
	@scala.annotation.tailrec
	def loop[T](clauses: List[List[T]], singulars: List[T] = Nil, mappings: List[(T, T)] = Nil): (List[List[T]], List[T], List[(T, T)]) = {
		if (clauses.isEmpty) {
			(clauses, singulars, mappings)
		} else {
			val (remainingClauses, newSingulars, newMappings) = round(clauses)
			if (newSingulars.isEmpty && newMappings.isEmpty) {
				(remainingClauses, singulars, mappings)
			} else {
				loop(remainingClauses, singulars ::: newSingulars, mergeCandidateDuplicates(mappings, newMappings))
			}
		}
	}

	/**
	  * Given some solutions and the information of duplicates, this method reconstructs all solutions that weren't detectable due to removing duplicate candidates.
	  * @param solutions The found solutions
	  * @param mappings The information of found duplicates
	  * @return All solutions that would have been found, if no duplicates were removed in the process
	  */
	@scala.annotation.tailrec
	def recoverCandidates[T](solutions: List[List[T]], mappings: List[(T, T)]): List[List[T]] = {
		if (mappings.isEmpty) {
			solutions
		} else {
			val map = mappings.head
			val duplicates = solutions.collect{
				case list if list.contains(map._1) => map._2 :: list.diff(List(map._1))
			}
			recoverCandidates(duplicates ::: solutions, mappings.tail)
		}
	}

	/**
	  * As the final step in the optimization process, this method takes any clauses that weren't solvable through non-destructive ways
	  * and solves them - if necessary - by brute force.
	  * In this process it is possible that new mappings are found and that completely different solutions (with different mappings) are found.
	  * These have to be applied within this method.
	  * @param clauses The remaining clauses to brute force
	  * @return All possible solutions
	  */
	def bruteForce[T](clauses: List[List[T]]): List[List[T]] = {
		val intersect = clauses.reduce(_ intersect _)
		if (!intersect.isEmpty) {
			List(intersect)
		} else {
			val candidates = clauses.flatten.distinct
			(2 to Math.min(candidates.size, clauses.size)).foldLeft((true, List[List[T]]())){
				case ((false, solutions), _) => (false, solutions)
				case ((true, solutions), size) => {
					val possibles = candidates.combinations(size).filter(sol => clauses.forall(!_.intersect(sol).isEmpty)).toList
					if (possibles.isEmpty) {
						(true, solutions)
					} else {
						(false, possibles)
					}
				}
			}._2
		}
	}

	def run(opponents: List[NamedBattler], candidates: List[NamedBattler], policy: MatchupPolicy): List[List[NamedBattler]] = {
		val (clauses, singulars, mappings) = loop(generateClauses(opponents, candidates, policy))
		val solutions = if (clauses.isEmpty) {
			List(singulars)
		} else {
			bruteForce(clauses).map(_ ::: singulars)
		}
		recoverCandidates(solutions, mappings)
	}
}
