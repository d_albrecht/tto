/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package challenge

import Definitions._

import policies._

object MatchupFormater {
	def formatOpponentPerspective(team: List[NamedBattler], opponents: List[NamedBattler], policy: MatchupPolicy): Unit = {
		println("Matchup-Formater - Opponent Perspective")
		opponents.foreach{ o =>
			println(s"Opponent: ${o.name}")
			val matchups = policy.suggest(o, team)
			matchups.foreach{ m =>
				println(s"\t${m.battler.evaluateMatchup(o.battler)}\t${m.name}")
			}
		}
		println
	}

	def formatTeamPerspective(team: List[NamedBattler], opponents: List[NamedBattler], policy: MatchupPolicy): Unit = {
		println("Matchup-Formater - Team Perspective")
		val matchup = opponents.map(o => (o, policy.suggest(o, team)))
		val reverse = team.map(m => {
			val filtered = matchup.filter(mu => {
				val counters = mu._2
				counters.contains(m)
			})
			(m, filtered.map(_._1))
		})
		reverse.foreach{ case (m, mu) =>
			println(s"Member: ${m.name}")
			mu.foreach{ o =>
				println(s"\t${m.battler.evaluateMatchup(o.battler)}\t${o.name}")
			}
		}
		println
	}
}
