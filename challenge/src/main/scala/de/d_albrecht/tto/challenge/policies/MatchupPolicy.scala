/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package challenge.policies

import Definitions._

/**
  * This interface models a strategy of which candidates from the given set is suitable to counter the given opponent.
  * 
  * This type sortof has an outward facing API and a different inward facing one. This is accomplished with the suggest-method.
  * Hence, you don't have to bother with the outward facing one at all.
  * 
  * To comply with the inward-facing one (the filter-method), you have to work according to the following principles:
  * - you can assume that you are given a non-empty set of candidates
  * - you are allowed to return no suitable candidates, but that should then be a conscious decision
  * - preferably you should try to provide some candidates
  * - in the case that you are not given any candidates, your implementation is allowed to fail with an exception
  * - when you are given any candidates, you should not fail but if necessary just return an empty list
  * - it is always the responsibility of the user to ensure that any policy is provided with candidates
  * - as it is the users choice to mixin any policy "plug-ins" that might reduce the candidates to zero
  */
trait MatchupPolicy {
	final def suggest(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
		val filtered = filter(opponent, candidates)
		if (filtered.isEmpty) {
			throw new NoSuchElementException("The policy wasn't able to find a single viable candidate!")
		} else {
			filtered
		}
	}
	def filter(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler]
}

/**
  * Queries the given policies for acceptable candidates as long as the previous policies don't provide any.
  * @param stages The policies to query
  */
class StagedMatchupPolicy(stages: MatchupPolicy*) extends MatchupPolicy {
	override def filter(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
		stages.foldLeft[List[NamedBattler]](Nil){
			case (Nil, pol) => pol.filter(opponent, candidates)
			case (res, _) => res
		}
	}
}

/**
  * Policy that just maxes out some kind of feature about the candidates.
  * @param ord The ordering for the type of feature to max
  */
trait MaxedMatchupPolicy[T](implicit ord: Ordering[T]) extends MatchupPolicy {
	override def filter(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
		val matchups: List[(NamedBattler, T)] = candidates.map(c => (c, eval(opponent, c)))
		val best = matchups.maxBy(_._2)._2
		val filtered = matchups.filter(_._2 == best)
		filtered.map(_._1)
	}
	def eval(opponent: NamedBattler, candidate: NamedBattler): T
}

class HighestAdvantage extends MaxedMatchupPolicy[Int] {
	override def eval(opponent: NamedBattler, candidate: NamedBattler): Int = {
		candidate.battler.evaluateMatchup(opponent.battler).sum
	}
}

class BestBalanced extends MaxedMatchupPolicy[(Int, Int)] {
	override def eval(opponent: NamedBattler, candidate: NamedBattler): (Int, Int) = {
		candidate.battler.evaluateMatchup(opponent.battler).sorted
	}
}

class BestOffense extends MaxedMatchupPolicy[(Int, Int)] {
	override def eval(opponent: NamedBattler, candidate: NamedBattler): (Int, Int) = {
		candidate.battler.evaluateMatchup(opponent.battler).offFirst
	}
}

class BestDefense extends MaxedMatchupPolicy[(Int, Int)] {
	override def eval(opponent: NamedBattler, candidate: NamedBattler): (Int, Int) = {
		candidate.battler.evaluateMatchup(opponent.battler).defFirst
	}
}

/**
  * Policy that returns all candidates that fulfill some boolean property.
  */
trait PropertyMatchupPolicy extends MatchupPolicy {
	override def filter(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
		val filtered = candidates.map(c => (c, fulfills(opponent, c))).filter(_._2)
		filtered.map(_._1)
	}
	def fulfills(opponent: NamedBattler, candidate: NamedBattler): Boolean
}

class DualAdvantage extends PropertyMatchupPolicy {
	override def fulfills(opponent: NamedBattler, candidate: NamedBattler): Boolean = {
		candidate.battler.evaluateMatchup(opponent.battler).min >= 1
	}
}

class NeutralPlus extends PropertyMatchupPolicy {
	override def fulfills(opponent: NamedBattler, candidate: NamedBattler): Boolean = {
		candidate.battler.evaluateMatchup(opponent.battler).min >= 0
	}
}

/**
  * As all policies are supposed to work with some given candidates, pre-eliminating some of the candidates can leave you with an empty candidate pool.
  * To prevent this when using any of these "plug-ins", each of these plug-ins should extend this interface and apply their criteria to this separate method.
  */
trait PreEliminatedMatchupPolicy extends MatchupPolicy {
	abstract override def filter(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
		val selected = select(opponent, candidates)
		if (selected.isEmpty) {
			Nil
		} else {
			super.filter(opponent, selected)
		}
	}

	def select(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = { candidates }
}

/**
  * BestDefense (and others) will favor any matchup where the team member is immune to the opponent,
  * even if that means that the team member can't (really) damage the opponent.
  * Mix in this trait, to ensure that the team member can at least do the given degree of damage to the opponent.
  * Use min=-5 to ensure any damage, regarding the actual factor.
  */
trait EnsureMinimumOffense(min: Int) extends PreEliminatedMatchupPolicy {
	abstract override def select(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
		super.select(opponent, candidates.filter(_.battler.evaluateMatchup(opponent.battler).offensive >= min))
	}
}

trait EnsureMinimumDefense(min: Int) extends PreEliminatedMatchupPolicy {
	abstract override def select(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
		super.select(opponent, candidates.filter(_.battler.evaluateMatchup(opponent.battler).defensive >= min))
	}
}

trait NoFourTimesWeakness extends PreEliminatedMatchupPolicy {
	abstract override def select(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
		super.select(opponent, candidates.filterNot(_.battler.defense.effectiveness.fourTimesWeakness))
	}
}

trait NoSelfWeakness extends PreEliminatedMatchupPolicy {
	abstract override def select(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
		super.select(opponent, candidates.filterNot(_.battler.defense.selfWeakness))
	}
}

trait OnlyDualType extends PreEliminatedMatchupPolicy {
	abstract override def select(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
		super.select(opponent, candidates.filter(_.battler.defense.comb.toList.distinct.size == 2))
	}
}
