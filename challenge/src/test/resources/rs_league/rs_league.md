Ruby/Sapphire League Challenge
==============================

To demonstrate the way you would use the ``challenge``-mode in practice, I chose the following scenario:

You face the league in Ruby/Sapphire for the first time. You have already started the game and are very close to the league (some decisions are already made) but other than that you have a wide range of options to build your league team. To limit the scope we further assume that you don't want or aren't able to trade, hence the regional dex is all there is. Furthermore, I excluded any legendaries because, frankly, I wanted to, but usually using legendaries is the easy way out and also usually works even if the types aren't fully favorable in some situations.

All the described steps are fully subjective and you might approach this differently, and that's totally legit as this type of optimization is a much more manual process compared to the ``game``-mode.

You can find test cases that demonstrate/validate most of the described observations in ``src/test/scala/de/d_albrecht/tto/resources/RubySapphireLeagueChallenge.scala``.

## Step 1: the opponents

The first step to get any hints for a good team is to collect the list of opponents. Based on publicly available information I created the csv-file "rs_league.csv". As all battlers on one side need to have unique names, and as it is quite common for a species to appear several times in one league (due to a limited regional dex), I differenciated some opponents by appending a number.

But in-game you might not be able to distinguish opponents of the same species easily (same level, same gender, same abilities if they activate). Hence, I "simplified" the created opponents list. To do so, I merged opponents of the same species by using a union of all attack types (abilities luckily were the same already). As hinted at before, some of these simplifications weren't necessary as the levels clearly distinguish some of the duplicates. (In that case you can also use details like the level or gender to differentiate the names of two or more opponents of the same species.) But even then, as the optimizer might try to suggest a different candidate for each opponent (depending on the used policy), having strictly less opponents will help create a team of reasonable size. Hence, any kind of simplification can help in the optimization process.

For most steps I will use the simplified list, but at any point you are free to check back with the full list to see, if treating opponents of the same species as different opponents might change the outcome.

To check for any obvious errors (typos) while collecting this information, I created a test each to load, parse and verify the two opponent lists for their size and uniqueness in regards to names.

## Step 2: the candidates

The second necessary list is the list of candidates, the regional dex. For that I collected all final stage species, together with their typing and - if applicable - abilities. If it is possible for one species to have two abilities where at least one alters the type effectiveness, I created two entries, one for each ability and with suitable names. To name the entries, I used their regional dex number and abbreviation of the ability where applicable.

As there are over one hundred final stage species, I didn't bother with any move sets. Instead I opted for regarding all STAB types as their attack types. That's a rough approximation, but usually it covers the basis of matchups quite well.

Similarly to the opponent file validation, I created a parsing/validation test for the candidate list.

## Step 3: the matchup policy

If you try the optimization with different policies, you will find out that, given that the challenge is facing 21 different opponents (some of which represent two actual opponents), most (highly) focused policies will suggest teams that are way to big for a generation 3 game. Using a more lax policy like "DualAdvantage" suggests teams of size 6. So, we will go with this.

Unfortunately, this policy relies on ``Shedinja``. That isn't a big problem under the right circumstances, but in this scenario, it would be devastating to actually rely on this frail species. In this specific case, the problematic aspect are weather conditions that could hurt and therefore kill a ``Shedinja`` in one turn. So, we have to exclude this one from any optimization. Hence, we have to adapt the optimization.

## Step 4: manual refinement

To do this, you have two options. Either, you can edit the candidate list file and simply remove the entry representing ``Shedinja``. Or, you go ahead and filter the parsed candidates programmatically to exclude specific entries. I used the latter option in order to keep the full list intact, but they are equally valid here.

Similarly you could observe that some teams contain candidates that you might not have access to. In this scenario it's mostly ``Swampert`` (one of the starters), but in the same situation you might encouter some optional species that you can miss and later on can't get to again, or you released the only one you got, you chose the wrong fosil/edition, etc. You might want to use this optimizer to see, which starter/fosil/edition to pick, or you are already focused on one team for most of your play through. In these cases, feel free to go ahead and exclude any exclusives as well. Although, if you go as far as to exclude any exclusives, you might as well create a separate dex list.

### Step 4a: creating a use-case specific policy

But then, after excluding ``Shedinja``, the smallest teams that the optimizer can find are of size 7, one entry too large. At this point you can go back and:

1. manually exclude more candidates. While this might seem counter-intuitive, but when using the right kind of policy this can actually help. Think it it this way, if you remove a candidate that would be the perfect counter for some opponent, then the optimization has to fall back to considering a complete other set of potential counters that might synergize with the counters of other opponents. But as I already use a rather lax policy in the first place, this effect would most likely not happen until I reduced the candidates to a very limited sub-set of the original dex, and therefore that would totally defeat the purpose of this optimizer.
2. manually exclude opponents. When you reduce the opponent list, the team suggestions become smaller as well. But that obviously would mean that the team wouldn't be able to effectively counter the challenge at hand. Hence, this is not the way to go (at least not blindly).
3. Use a completely different policy. At some point you have to commit to some policy. Otherwise, you are playing around with too many options and won't find a way out of the rabbit hole you got yourself into. At an early stage, you still can weigh up your options. For this example, let's assume that exchanging the policy completely isn't a solution either.
4. manually inspect the results to find patterns (this involves the ``MatchupFormatter`` as described in step 6, so if you want to reproduce this step by step, jump forward and check this step now, or learn about this tool when you have found your team already). After excluding ``Shedinja`` all teams have seven members. And using the MatchupFormatter you can see that in almost all suggestions there is one team member solely selected in order to face ``Metagross``. It isn't the only opponent that in all these team suggestions is always assigned to exactly one candidate alone, but all other candidates are more versatile. In some of the teams, the ``Metagross`` counter is also able to face some other opponent (namely ``Armaldo``), but this additional opponent is also assignable to some other team member with an equally good matchup evaluation. So, just because of ``Metagross`` the teams have to be of size seven. This in general is unfortnate, but as this is one out of 26 opponents, we can use this to push the optimizer into generating smaller teams.

In order to do so, we slightly adapt the used policy. If the policy is asked for candidates for ``Metagross``, we use an even more lax criterion, otherwise we apply the already used ``DualAdvantage`` (see ``MetagrossPolicy`` in the test). This surely has the drawback that we specifically relaxed the policy for the opponent with the highest level. But as every battle isn't won by types alone, what would a challenge be if it wasn't still somewhat challenging. After all, battles are about strategy most of the time. And that's something this project can't help you with at all.

### Step 4b: choosing a team

With all that in place, we are left with five teams of size six, where five candidates are present in all teams and the sixth one differs (one of the five candidates for the sixth spot being of the version/choice exclusives kind of candidates). Essentially, five of the six team members are now chosen by the optimizer, and it is up to us to pick the sixth. But that doesn't mean, that the team has to be perfect right now. It is still advisable to inspect the suggested candidates and look for potential weak spots you haven't considered yet. Like a bad ability or a low base stat total. Fortunately, the five fixed team members are actually quite decent. But these criteria can also help to select the sixth.

Again, based on observation, I saw that all five of the necessary candidates are dual types. So, you might want to use the ``OnlyDualType`` policy "plug-in" and limit all teams to dual types (leaving you with three options for the sixth member, two that aren't exclusives). Or you can take a look at move sets as well.

I mainly looked at base stat totals and therefore ended up with the team ``#35 Breloom``, ``#72 Aggron``, ``#83 Magneton``, ``#115 Skarmory``, ``#175 Walrein``, and finally my own pick of ``#130 Crawdaunt``.

## Step 5: team move sets

Besides the fact that you still have to come up with an actual strategy, remember when I said, STAB attacks is a good approximation for move set? Now is the point to recify this. Because while that can't really change the optimizer's judgement for the most parts, it is an essential information for the actual battles.

Similarly to the opponents list, I created another csv-file that only contained my team of six, but this time I took my time and looked into their learn set. What do they learn by level up, what can you breed onto them (given you have access to the necessary moves and fathers in your game), what TMs you still own (or can purchase again). The nice thing here is, that you don't have to fully commit to one exact move set yet, you just need the types you consider. And you can also take more than four types at first. Because (a) feel free to play around with different move sets and (b) at least for TM moves you can change them halfway through the challenge, you only have to buy them beforehand.

In the case that some team member can't learn any move of one of their own types (or of their only type for mono types) and hence can't (fully) utilize the assumed STAB effects, you can modify the original dex file and re-run the optimization. But (a) that is rather rare and (b) you rarely are left with a complete disadvantage in one match even if you can't use STAB. You will see the effect of your move set choice in the next step and you can always come back and change anything at any stage if you are not satisfied yet, but making smaller changes is always advisable because it won't invalidate too much of the already created results.

For a more conservative approach, I limited myself to level up moves of the evolutionary line, HMs and egg moves, as TMs might not be available in all cases. That leaves five different attack types for more than half of the team. To reduce this to an actual usable team with four attacks (at most), I removed ``Normal`` types from the list wíthout changing anything in the matchup (see step 6). Including ``Normal`` moves in the first place doesn't benefit you much usually, but in certain situations it can make the difference between not being able to hit an opponent at all to at least having a neutral move at your disposal. Removing ``Normal`` in this scenario leaves me with a ``Fighting`` type move with base power 20 for some team members, so in reality keeping stronger ``Normal``-type moves and removing the weaker ``Fighting`` move might be benefitial. But I will prefer the (potential) type advantage here. If you want to utilize ``Status`` moves as well, you have to limit yourself to at most three attack types here.

Either way, you might want to create a parsing/validation test for these new files as well (just in case).

## Step 6: counter matrix

At this point you have the species to catch/breed, the abilities in the cases where they are significant for the matchup, and a rough move set. You might as well, try your luck and face the challenge at hand based on own intuition.

But this project can at least do one more step for you. It can explicitly tell you when to switch to which team members. Effectively creating a matrix of opponents and their coutners. That's the purpose of the ``MatchupFormatter``. It won't give you a strict switching guide, but it will at least narrow down the options for you by doing the type effectiveness calculations. For example it will still be up to you to decide if a better offense or defense matches your style more and in case the same sub-set is possible for several opponents that you switch around a bit and not use the same team member(s) all the time.

First of all, I encourage you to try out using different policies at this stage with the formatter. While some (laxer) are better to select a team because they utilize synergies more, other (more stricter) will give you a more sparse/specific counter matrix. ``BestBalanced`` is usually a good policy in this step, but you might be better off with a different one, depending on your play style besides other factors. And in case all suggested counters are incapacitated to some extend, using the formatter with a laxer policy will show you other options with their viability.

And at this stage you want to use the original opponents list as well. Either instead of the simplified one, or in addition. This will tell you to which degree identifying the possible duplicate can influence your strategy and which team member to send out when you haven't identified the duplicate yet. The simplified list had artificial opponents after all, that - in this form - you will never face.

### The two modes of the formatter

The formatter has two options. One is more for taking on the actual challenge. In this mode the matchup is categorized by the opponents. Hence, if some opponent is switched in, you can look it up and see, which team member(s) to switch to. The other option is the more analytical one. In this mode the matchup is listed by team members. If in this mode you see that some team member seems under-utilized but the opponents they are selected for are also listed for other team members, then go ahead and assign this under-utilized member to all their possible opponents and therefore prefer this team member over the other possible counters. This way you can slowly but surely create a good (as in sparse) counter matrix by eliminating duplicate entries, leaving less chance for errors in the actual battles.

While playing around with different TM move types, I had the situation that one team member could only ever be used (effectively) against two out of 26 opponents. Unfortunately, I can't reproduce this anymore. But in this case you would strictly assign these opponents to this team member. Otherwise this team member wouldn't have been used at all and you could have used one team mmber less for the challenge.

Another observation I had was that when you re-run the optimizer with a realistic move set, the optimizer was able to even reduce the team size to five despite using a strict matchup policy. So, you might as well decide to introduce move sets earlier in the process (for example easily as early as in step 4a, perhaps even in step 3 after you reduced the potential team members to a dozen or so). But, using smaller teams for the league doesn't earn you a badge of any sort. In fact, it makes the league even harder (because of PP management for example). So to not redo everything, I kept the team of six to have fall back team members in case some battle doesn't play out the way I planned.

Feel free to also examine how a portion of the challenge can play out. In this scenario, the "challenge" is a whole league, consisting of five battles. Your challenge could also be an evil team base, then each individual grunt could be an interesting scope. Or a challenge could be all eight gyms (or gym leaders) of one game, then one gym would be such a sub-set. Let's take the champion in this scenario as one portion and check the usage of the team, and whether some members might be over-used.

You will see that ``Walrein`` isn't useful here at all (at least not effectively). If you check the elite four members, it was useful for ``Dark``, ``Ice`` and ``Dragon``, so it deserves a rest. All the other team members have something to do here, although it is up to you and their condition, if you want to use ``Aggron`` and ``Crawdaunt`` as ``Skarmory`` can substitute them perfectly. But everything else being equal, I would probably try to have one opponent per team member except for ``Breloom`` who has to face two. And knowing the first opponent that this trainer will send out, you should put the right counter in front before the battle. Similarly, you should check each such portion, and map out a rough strategy, for example against the ``Ice`` specialist. And whether you want to try to identify duplicate opponents rather fast or if using the safe fall back to handle all duplicates is good enough.

## Final Thoughts

The optimizer is a tool, nothing more. If you build a team for such a challenge yourself, you would usually do the exact same steps the optimizer does, but less systematically. Still, you would see that there is a ``Dark`` type specialist and look for ``Fighting`` type team members, ``Dark`` for their ``Ghost`` and ``Ice`` for their ``Dragon``. And as you could see, the optimizer is highly dependent on your input and adjustments. The advantage you have using the optimizer is that you can't really miss some specific and good matchups because the optimizer will try them all.

That means that at times you have to prove your creativity, as we could see with the ``Metagross`` example. And the order of the steps isn't set in stone either.

But as you can observe in this scenario, relaxing the ``Metagross`` policy wasn't as devastating as feared in the beginning. Because as it turns out, even against ``Metagross`` there is a team member that still has a slight type advantage against this opponent. We could have ensured this by exactly dialing the amount we want to relax the general policy. But whether this works or not depends on the exact case you are tackling.