/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package challenge

class ChallengeOptimizerTest extends TestBase {
	"ChallengeOptimizer" should {
		"extractSingularClauses" in {
			val clauses = List(List(1), List(2), List(3), List(4, 5), List(3, 6), List(5, 6), List(1, 2, 3), List(1, 2, 4), List(2, 4, 6), List(4, 5, 6))
			val expected = (List(List(4, 5), List(5, 6), List(4, 5, 6)), List(1, 2, 3))

			val uut = ChallengeOptimizer

			uut.extractSingularClauses(clauses) should be (expected)
		}

		"fail on extractSingularClauses" in {
			assertThrows[IllegalStateException] {
				val clauses = List(List())

				val uut = ChallengeOptimizer

				uut.extractSingularClauses(clauses)
			}
		}

		"reduceClauses" in {
			val clauses = List(List(1), List(1), List(2), List(2, 3), List(3, 4), List(4, 5), List(3, 5), List(4, 5, 6, 7))
			val expected = List(List(1), List(2), List(3, 4), List(4, 5), List(3, 5))

			val uut = ChallengeOptimizer

			uut.reduceClauses(clauses) should be (expected)
		}

		"reduceCandidates" in {
			val clauses = List(List(1, 2), List(3, 4, 5), List(3, 4, 6), List(1, 2, 5), List(1, 2, 6), List(7, 8, 9, 10), List(7, 8, 9, 11))
			val expected = (List(List(1), List(3, 5), List(3, 6), List(1, 5), List(1, 6), List(7, 10), List(7, 11)), List((1, 2), (3, 4), (7, 8), (7, 9)))

			val uut = ChallengeOptimizer

			uut.reduceCandidates(clauses) should be (expected)
		}

		"mergeCandidateDuplicates" in {
			val oldDups = List((1, 2), (1, 3), (4, 5), (4, 6), (7, 8))
			val newDups = List((1, 7), (9, 4), (10, 11))
			val expected = List((1, 2), (1, 3), (9, 5), (9, 6), (1, 8), (1, 7), (9, 4), (10, 11))

			val uut = ChallengeOptimizer

			uut.mergeCandidateDuplicates(oldDups, newDups) should be (expected)
		}

		"fail on mergeCandidateDuplicates" in {
			assertThrows[IllegalArgumentException] {
				val oldDups = List((1, 2), (1, 3), (4, 5), (4, 6), (7, 8))
				val newDups = List((1, 7), (9, 4), (10, 11), (12, 4))

				val uut = ChallengeOptimizer

				uut.mergeCandidateDuplicates(oldDups, newDups)
			}
		}

		"round" in {
			val clauses = List(List(1), List(1), List(2), List(3), List(4, 5, 6), List(2, 5), List(3, 4), List(1, 2, 3), List(1, 3, 5), List(7, 8, 9), List(4, 5, 6, 9), List(7, 8, 10))
			val expected = (List(List(4), List(7, 9), List(7, 10)), List(1, 2, 3), List((4, 5), (4, 6), (7, 8)))

			val uut = ChallengeOptimizer

			uut.round(clauses) should be (expected)
		}

		"loop" in {
			val clauses = List(List(1), List(1), List(2), List(3), List(4, 5, 6), List(2, 5), List(3, 4), List(1, 2, 3), List(1, 3, 5), List(7, 8, 9), List(4, 5, 6, 9), List(7, 8, 10))
			val expected = (List(List(7, 9), List(7, 10)), List(1, 2, 3, 4), List((4, 5), (4, 6), (7, 8)))

			val uut = ChallengeOptimizer

			uut.loop(clauses) should be (expected)
		}

		"recoverCandidates" in {
			val solutions = List(List(1, 5, 8))
			val mappings = List((1, 2), (1, 3), (1, 4), (5, 6), (5, 7), (8, 9))
			val expected = List(Set(1, 5, 8), Set(2, 5, 8), Set(3, 5, 8), Set(4, 5, 8), Set(1, 6, 8), Set(2, 6, 8), Set(3, 6, 8), Set(4, 6, 8), Set(1, 7, 8), Set(2, 7, 8), Set(3, 7, 8), Set(4, 7, 8),
				Set(1, 5, 9), Set(2, 5, 9), Set(3, 5, 9), Set(4, 5, 9), Set(1, 6, 9), Set(2, 6, 9), Set(3, 6, 9), Set(4, 6, 9), Set(1, 7, 9), Set(2, 7, 9), Set(3, 7, 9), Set(4, 7, 9))

			val uut = ChallengeOptimizer

			expected.toSet.size should be (expected.size)
			uut.recoverCandidates(solutions, mappings).size should be (expected.size)
			uut.recoverCandidates(solutions, mappings).map(_.toSet).toSet should be (expected.toSet)
		}

		"bruteForce" in {
			val clauses = List(List(1, 2), List(1, 3), List(2, 3))
			val expected = List(List(1, 2), List(1, 3), List(2, 3))

			val uut = ChallengeOptimizer

			uut.bruteForce(clauses) should be (expected)
		}

		"bruteForce complex" in {
			val clauses = List(List(1, 2), List(2, 3), List(3, 4), List(4, 5), List(5, 1))
			val expected = List(List(1, 2, 4), List(1, 3, 4), List(1, 3, 5), List(2, 3, 5), List(2, 4, 5))

			val uut = ChallengeOptimizer

			uut.bruteForce(clauses) should be (expected)
		}

		"bruteForce simple" in {
			val clauses = List(List(1, 2), List(1, 3), List(1, 4))
			val expected = List(List(1))

			val uut = ChallengeOptimizer

			uut.bruteForce(clauses) should be (expected)
		}
	}
}
