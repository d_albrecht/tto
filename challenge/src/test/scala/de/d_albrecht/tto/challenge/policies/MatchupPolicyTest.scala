/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package challenge.policies

import Definitions._

import types._

import TestCombinations._

class MatchupPolicyTest extends TestBase {
	class BlankPolicy extends MatchupPolicy {
		override def filter(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = candidates
	}

	def asCandidates(matrix: TypeMatrix, candidates: List[(PkmnType, PkmnType)]): List[NamedBattler] = {
		val combinations = candidates.map(matrix.combination(_))
		combinations.map(comb => comb.asBattler(comb.readable.toString()))
	}

	def asOpponent(matrix: TypeMatrix, opponent: (PkmnType, PkmnType)): NamedBattler = {
		val combination = matrix.combination(opponent)
		combination.asBattler(combination.readable.toString())
	}

	"StagedMatchupPolicy" should {
		"suggest candidates" in {
			val matrix = TypeMatrices.gen2
			val opponent = asOpponent(matrix, SABLEYE)
			val candidates = asCandidates(matrix, List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU, GOLEM))

			val uut = new StagedMatchupPolicy(new DualAdvantage, new HighestAdvantage with EnsureMinimumOffense(1), new NeutralPlus with EnsureMinimumDefense(1), new BestBalanced)

			uut.suggest(opponent, candidates) should be (candidates)
		}

		"fail to suggest candidates" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen2
				val opponent = asOpponent(matrix, SABLEYE)
				val candidates = asCandidates(matrix, List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU, GOLEM))

				val uut = new StagedMatchupPolicy(new DualAdvantage, new HighestAdvantage with EnsureMinimumOffense(1), new NeutralPlus with EnsureMinimumDefense(1))

				uut.suggest(opponent, candidates)
			}
		}
	}

	"HighestAdvantage" should {
		"suggest candidates" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, MAGNEMITE)
			val candidates = asCandidates(matrix, List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU, GOLEM))
			val expected = List(4).map(candidates(_))

			val uut = new HighestAdvantage

			uut.suggest(opponent, candidates) should be (expected)
		}
	}

	"BestBalanced" should {
		"suggest candidates" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, CAMERUPT)
			val candidates = asCandidates(matrix, List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU, GOLEM))
			val expected = List(2).map(candidates(_))

			val uut = new BestBalanced

			uut.suggest(opponent, candidates) should be (expected)
		}
	}

	"BestOffense" should {
		"suggest candidates" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, MURKROW)
			val candidates = asCandidates(matrix, List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU, GOLEM))
			val expected = List(3, 4).map(candidates(_))

			val uut = new BestOffense

			uut.suggest(opponent, candidates) should be (expected)
		}
	}

	"BestDefense" should {
		"suggest candidates" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, LEAFEON)
			val candidates = asCandidates(matrix, List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU, GOLEM))
			val expected = List(0).map(candidates(_))

			val uut = new BestDefense

			uut.suggest(opponent, candidates) should be (expected)
		}
	}

	"DualAdvantage" should {
		"suggest candidates" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, LUCARIO)
			val candidates = asCandidates(matrix, List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU, GOLEM))
			val expected = List(1).map(candidates(_))

			val uut = new DualAdvantage

			uut.suggest(opponent, candidates) should be (expected)
		}

		"fail to suggest candidates" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen6
				val opponent = asOpponent(matrix, MAGIKARP)
				val candidates = asCandidates(matrix, List(EEVEE, PIDGEY, GASTLY, GLACEON, PAWNIARD, RAYQUAZA, ESPEON, GOLEM, TOGEKISS, CHARIZARD, GARCHOMP))

				val uut = new DualAdvantage

				uut.suggest(opponent, candidates)
			}
		}

		"suggest candidates for almost all combinations" in {
			val matrix = TypeMatrices.gen6
			val candidates = matrix.allUniqueCombinations.map(c => c.asBattler(c.readable.toString()))

			val uut = new DualAdvantage

			for (opponent <- candidates if opponent.name != "(Poison,Water)" && opponent.name != "(Flying,Ground)") {
				uut.suggest(opponent, candidates).isEmpty should be (false)
			}
		}

		"fail to suggest candidates for (Poison,Water)" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen6
				val opponent = asOpponent(matrix, ("Poison", "Water").typeIds)
				val candidates = matrix.allUniqueCombinations.map(c => c.asBattler(c.readable.toString()))

				val uut = new DualAdvantage

				uut.suggest(opponent, candidates)
			}
		}

		"fail to suggest candidates for (Flying,Ground)" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen6
				val opponent = asOpponent(matrix, ("Flying", "Ground").typeIds)
				val candidates = matrix.allUniqueCombinations.map(c => c.asBattler(c.readable.toString()))

				val uut = new DualAdvantage

				uut.suggest(opponent, candidates)
			}
		}
	}

	"NeutralPlus" should {
		"suggest candidates" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, PARASECT)
			val candidates = asCandidates(matrix, List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU, GOLEM))
			val expected = List(0, 1).map(candidates(_))

			val uut = new NeutralPlus

			uut.suggest(opponent, candidates) should be (expected)
		}

		"suggest candidates for all combinations" in {
			val matrix = TypeMatrices.gen6
			val candidates = matrix.allUniqueCombinations.map(c => c.asBattler(c.readable.toString()))

			val uut = new NeutralPlus

			for (opponent <- candidates) {
				uut.suggest(opponent, candidates).isEmpty should be (false)
			}
		}

		"fail to suggest candidates" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen6
				val opponent = asOpponent(matrix, PAWNIARD)
				val candidates = asCandidates(matrix, List(EEVEE, PIDGEY, GASTLY, BULBASAUR, GLACEON, PAWNIARD, RAYQUAZA, ESPEON, GOLEM, TOGEKISS))

				val uut = new NeutralPlus

				uut.suggest(opponent, candidates)
			}
		}
	}

	"EnsureMinimumOffense" should {
		"not pass on Normal types against Ghost types" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen6
				val opponent = asOpponent(matrix, GASTLY)
				val candidates = asCandidates(matrix, List(EEVEE))

				val uut = new BlankPolicy with EnsureMinimumOffense(-5)

				uut.suggest(opponent, candidates)
			}
		}

		"not pass on Normal types against Steel types" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen6
				val opponent = asOpponent(matrix, MAGNEMITE)
				val candidates = asCandidates(matrix, List(EEVEE))

				val uut = new BlankPolicy with EnsureMinimumOffense(0)

				uut.suggest(opponent, candidates)
			}
		}

		"pass on Electric types against Gyarados" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, GYARADOS)
			val candidates = asCandidates(matrix, List(MAGNEMITE))

			val uut = new BlankPolicy with EnsureMinimumOffense(2)

			uut.suggest(opponent, candidates) should be (candidates)
		}
	}

	"EnsureMinimumDefense" should {
		"not pass on Steel types against Fire types" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen6
				val opponent = asOpponent(matrix, FLAREON)
				val candidates = asCandidates(matrix, List(MAGNEMITE))

				val uut = new BlankPolicy with EnsureMinimumDefense(0)

				uut.suggest(opponent, candidates)
			}
		}

		"pass on Ghost types against Normal types" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, EEVEE)
			val candidates = asCandidates(matrix, List(GASTLY))

			val uut = new BlankPolicy with EnsureMinimumDefense(5)

			uut.suggest(opponent, candidates) should be (candidates)
		}
	}

	"NoFourTimesWeakness" should {
		"pass on Bulbasaur in gen 6+" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, EEVEE)
			val candidates = asCandidates(matrix, List(BULBASAUR))

			val uut = new BlankPolicy with NoFourTimesWeakness

			uut.suggest(opponent, candidates) should be (candidates)
		}

		"not pass on Bulbasaur in gen 1" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen1
				val opponent = asOpponent(matrix, EEVEE)
				val candidates = asCandidates(matrix, List(BULBASAUR))

				val uut = new BlankPolicy with NoFourTimesWeakness

				uut.suggest(opponent, candidates)
			}
		}

		"not pass on Gyarados" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen6
				val opponent = asOpponent(matrix, EEVEE)
				val candidates = asCandidates(matrix, List(GYARADOS))

				val uut = new BlankPolicy with NoFourTimesWeakness

				uut.suggest(opponent, candidates)
			}
		}
	}

	"NoSelfWeakness" should {
		"pass on Magikarp" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, EEVEE)
			val candidates = asCandidates(matrix, List(MAGIKARP))

			val uut = new BlankPolicy with NoSelfWeakness

			uut.suggest(opponent, candidates) should be (candidates)
		}

		"not pass on particular Ghosts" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen6
				val opponent = asOpponent(matrix, EEVEE)
				val candidates = asCandidates(matrix, List(GASTLY))

				val uut = new BlankPolicy with NoSelfWeakness

				uut.suggest(opponent, candidates)
			}
		}
	}

	"OnlyDualType" should {
		"pass on Charizard" in {
			val matrix = TypeMatrices.gen6
			val opponent = asOpponent(matrix, EEVEE)
			val candidates = asCandidates(matrix, List(CHARIZARD))

			val uut = new BlankPolicy with OnlyDualType

			uut.suggest(opponent, candidates) should be (candidates)
		}

		"not pass on Pikachu" in {
			assertThrows[NoSuchElementException] {
				val matrix = TypeMatrices.gen6
				val opponent = asOpponent(matrix, EEVEE)
				val candidates = asCandidates(matrix, List(PIKACHU))

				val uut = new BlankPolicy with OnlyDualType

				uut.suggest(opponent, candidates)
			}
		}
	}
}
