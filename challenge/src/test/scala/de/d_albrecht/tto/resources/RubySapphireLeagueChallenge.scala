/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package resources

import Definitions._

import challenge._
import policies._
import types._

import scala.io.Source

/**
  * see src/test/resources/rs_league/ for the used/necessary resource files and a description
  */
class RubySapphireLeagueChallenge extends TestBase {
	val PATH = "./challenge/src/test/resources/rs_league/"
	val MATRIX = TypeMatrices.gen2

	def loadWithAttacks(file: String): List[NamedBattler] = {
		TypeCombinationParser.parseLines(Source.fromFile(PATH + file).getLines().toList).map(_.asNamedBattler(MATRIX))
	}
	def loadWithStab(file: String): List[NamedBattler] = {
		TypeCombinationParser.parseLines(Source.fromFile(PATH + file).getLines().toList, ParserParameters(parseAttacks = false)).map(_.withStab().asNamedBattler(MATRIX))
	}

	trait MetagrossPolicy extends MatchupPolicy {
		abstract override def filter(opponent: NamedBattler, candidates: List[NamedBattler]): List[NamedBattler] = {
			if (opponent.name == "Metagross") {
				// We could implement this here again, but as every policy has to return SOME viable candidates,
				// re-using an existing and tested implementation is saver.
				new NeutralPlus().filter(opponent, candidates)
			} else {
				super.filter(opponent, candidates)
			}
		}
	}

	"RubySapphireLeague" should {
		"step 1: parse ruby/sapphire league" in {
			val lines = Source.fromFile(PATH + "rs_league.csv").getLines().toList

			val parsed = TypeCombinationParser.parseLines(lines)

			// league file contains four empty lines as visual separators
			parsed.size should be (lines.size - 4)
			parsed.map(_.name).distinct.size should be (lines.size - 4)
		}

		"step 1: parse simplified ruby/sapphire league" in {
			val lines = Source.fromFile(PATH + "rs_league_simplified.csv").getLines().toList

			val parsed = TypeCombinationParser.parseLines(lines)

			// league file contains four empty lines as visual separators
			parsed.size should be (lines.size - 4)
			parsed.map(_.name).distinct.size should be (lines.size - 4)
		}

		"step 2: parse ruby/sapphire dex" in {
			val lines = Source.fromFile(PATH + "rs_dex.csv").getLines().toList

			val parsed = TypeCombinationParser.parseLines(lines, ParserParameters(parseAttacks = false))

			parsed.size should be (lines.size)
			parsed.map(_.name).distinct.size should be (lines.size)
		}

		"step 3: initial try with too specific matchup policy" in {
			val opponents = loadWithAttacks("rs_league.csv")
			val candidates = loadWithStab("rs_dex.csv")
			val policy = new BestOffense()

			val teams = ChallengeOptimizer.run(opponents, candidates, policy)

			// there are 48 suggested teams
			teams.size should be (48)
			// all of size nine
			teams.map(_.size).distinct should be (List(9))
			// all utilizing Shedinja
			teams.forall(_.exists(_.name == "44")) should be (true)
			// six members should be present in each team
			teams.reduce(_.intersect(_)).map(_.name).size should be (6)
		}

		"step 3: initial try with decent matchup policy" in {
			val opponents = loadWithAttacks("rs_league_simplified.csv")
			val candidates = loadWithStab("rs_dex.csv")
			val policy = new StagedMatchupPolicy(new DualAdvantage(), new BestBalanced())

			val teams = ChallengeOptimizer.run(opponents, candidates, policy)

			// there are five suggested teams
			teams.size should be (5)
			// all of size six
			teams.map(_.size).distinct should be (List(6))
			// all utilizing Shedinja
			teams.forall(_.exists(_.name == "44")) should be (true)
			// five members should be present in each team
			teams.reduce(_.intersect(_)).map(_.name).size should be (5)
		}

		"step 4: suggestions without version/choice exclusives" in {
			val opponents = loadWithAttacks("rs_league_simplified.csv")
			val exclusives = List("3", "6", "9", "21", "24", "68", "69", "123", "124", "125", "126", "134", "136")
			val candidatesRaw = loadWithStab("rs_dex.csv")
			val candidates = candidatesRaw.filterNot(c => exclusives.contains(c.name))
			candidates.size should be (candidatesRaw.size - exclusives.size)
			val policy = new StagedMatchupPolicy(new DualAdvantage(), new BestBalanced())

			val teams = ChallengeOptimizer.run(opponents, candidates, policy)

			// there are still four suggested teams
			teams.size should be (4)
			// all of size six
			teams.map(_.size).distinct should be (List(6))
			// all utilizing Shedinja
			teams.forall(_.exists(_.name == "44")) should be (true)
		}

		"step 4a: suggestions without Shedinja and exclusives" in {
			val opponents = loadWithAttacks("rs_league_simplified.csv")
			val exclusives = List("3", "6", "9", "21", "24", "44", "68", "69", "123", "124", "125", "126", "134", "136")
			val candidatesRaw = loadWithStab("rs_dex.csv")
			val candidates = candidatesRaw.filterNot(c => exclusives.contains(c.name))
			candidates.size should be (candidatesRaw.size - exclusives.size)
			val policy = new StagedMatchupPolicy(new DualAdvantage(), new BestBalanced())

			val teams = ChallengeOptimizer.run(opponents, candidates, policy)

			// there are now 20 suggested teams
			teams.size should be (20)
			// all of size seven
			teams.map(_.size).distinct should be (List(7))
			// with one team member specifically for Metagross
			teams.forall{ team =>
				val metagrossCounter = new BestBalanced().suggest(opponents.filter(_.name == "Metagross").head, team)
				// Metagross has only one counter and
				metagrossCounter.size should be (1)
				opponents.forall{ opp =>
					def hasAlternatives(counter: List[NamedBattler]): Boolean = {
						// for counter groups with the one counter for Metagross there is an alternative just as good
						!counter.contains(metagrossCounter.head) || counter.size > 1
					}
					opp.name == "Metagross" || hasAlternatives(new BestBalanced().suggest(opp, team))
				}
			} should be (true)
		}

		"step 4b: finding teams without Shedinja or exclusives, relaxing the condition for Metagross" in {
			val opponents = loadWithAttacks("rs_league_simplified.csv")
			val exclusives = List("3", "6", "9", "21", "24", "44", "68", "69", "123", "124", "125", "126", "134", "136")
			val candidatesRaw = loadWithStab("rs_dex.csv")
			val candidates = candidatesRaw.filterNot(c => exclusives.contains(c.name))
			candidates.size should be (candidatesRaw.size - exclusives.size)
			val policy = new StagedMatchupPolicy(new DualAdvantage(), new BestBalanced()) with MetagrossPolicy

			val teams = ChallengeOptimizer.run(opponents, candidates, policy)

			// there are again four suggested teams
			teams.size should be (4)
			// all of size six
			teams.map(_.size).distinct should be (List(6))
			// five members should be present in each team
			val commons = teams.reduce(_.intersect(_)).toSet
			commons.map(_.name) should be (Set("35", "72", "83", "115", "175"))
			// and I can choose from these four to fill the last spot
			teams.reduce(_.concat(_)).toSet.diff(commons).map(_.name) should be (Set("11", "98", "130", "152"))
		}

		"step 4b: finding teams without Shedinja or exclusives and only dual types, relaxing the condition for Metagross" in {
			val opponents = loadWithAttacks("rs_league_simplified.csv")
			val exclusives = List("3", "6", "9", "21", "24", "44", "68", "69", "123", "124", "125", "126", "134", "136")
			val candidatesRaw = loadWithStab("rs_dex.csv")
			val candidates = candidatesRaw.filterNot(c => exclusives.contains(c.name))
			candidates.size should be (candidatesRaw.size - exclusives.size)
			val policy = new StagedMatchupPolicy(new DualAdvantage(), new BestBalanced()) with MetagrossPolicy with OnlyDualType

			val teams = ChallengeOptimizer.run(opponents, candidates, policy)

			// there are only two suggested teams left
			teams.size should be (2)
			// all of size six
			teams.map(_.size).distinct should be (List(6))
			// five members should be present in each team
			val commons = teams.reduce(_.intersect(_)).toSet
			commons.map(_.name) should be (Set("35", "72", "83", "115", "175"))
			// and I can choose from these two to fill the last spot
			teams.reduce(_.concat(_)).toSet.diff(commons).map(_.name) should be (Set("98", "130"))
		}

		"step 5: parse league team" in {
			val lines = Source.fromFile(PATH + "rs_league_team.csv").getLines().toList

			val parsed = TypeCombinationParser.parseLines(lines)

			parsed.size should be (lines.size)
			parsed.map(_.name).distinct.size should be (lines.size)
		}

		"step 5: parse slimmed league team" in {
			val lines = Source.fromFile(PATH + "rs_league_team_slimmed.csv").getLines().toList

			val parsed = TypeCombinationParser.parseLines(lines)

			parsed.size should be (lines.size)
			parsed.map(_.name).distinct.size should be (lines.size)
		}

		"step 6: find a smaller team with attack types against full opponent list" in {
			val opponents = loadWithAttacks("rs_league.csv")
			val candidates = loadWithAttacks("rs_league_team_slimmed.csv")
			val policy = new BestBalanced()

			val teams = ChallengeOptimizer.run(opponents, candidates, policy)

			// there are two suggested teams
			teams.size should be (2)
			// all of size five
			teams.map(_.size).distinct should be (List(5))
			// four members would be present in each team
			val commons = teams.reduce(_.intersect(_)).toSet
			commons.map(_.name) should be (Set("Skarmory", "Walrein", "Magneton", "Breloom"))
			// and I can choose from these two to fill the last spot
			teams.reduce(_.concat(_)).toSet.diff(commons).map(_.name) should be (Set("Aggron", "Crawdaunt"))

		}

		"part 6: check switch in suggestions for elite four battle" in {
			val opponents = loadWithAttacks("rs_league.csv").filter(_.battler.defense.comb.toList.contains("Ice".typeId))
			val team = loadWithAttacks("rs_league_team_slimmed.csv")
			val policy = new BestBalanced()

			MatchupFormater.formatOpponentPerspective(team, opponents, policy)
		}

		"part 6: check team usage while champion battle" in {
			val opponentsRaw = loadWithAttacks("rs_league.csv")
			// you could just drop the first 20 entries, but this works just as well as none elite four types are used by the champion
			// and this would also work for the simplified version without having to double check the number of entries to drop
			val excludes = List("Dark", "Ghost", "Ice", "Dragon").map(_.typeId)
			val opponents = opponentsRaw.filter(_.battler.defense.comb.toList.intersect(excludes).isEmpty)
			val team = loadWithAttacks("rs_league_team_slimmed.csv")
			val policy = new BestBalanced()

			MatchupFormater.formatTeamPerspective(team, opponents, policy)
		}
	}
}
