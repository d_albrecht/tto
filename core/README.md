Team Type Optimizer Core
========================

This project provides core definitions to be used by any concrete optimizing project for specific aspects in the realm of this optimizer concept. Therefore, this project itself doesn't offer any runnable applications. Still, the idea behind most of the offered functions is to hide away any technical details in this project. Hence, I will explain how I ended up with the approaches I used but mostly you won't need to know the implementation to use the optimizing part.

## Order of types

While you can find several tables of type effectiveness on the internet and they might even be matching the game's internal code, I never understood the ordering. So, I reordered them. Which in reality should not be a problem for any interaction with the project because for the most part, to the outside world (anything outside the ``core``-project) the ordering - or even the fact that types are just plain ``Integer``s isn't really visible. I added ways to convert the string representation of a type to their id in this project and that's the intended way to use the types.

But, to at least explain why I came up with the ordering I use, here are my criteria:

The order is a combination of several principles:
- First, all types are divided into the generations they were introduced. If a type got introduced later on, it will be farther down the list.
- Then, the types are split into originally physical and originally special types (as most type lists still do that). This concept got removed after generation 3, but except for the ``Fairy`` type (that was the only type introduced after this concept of physical and special types got removed, so doesn't need any further ordering) all types still hold some remembrance of this split. And the ``Fairy`` type got assigned there, too, to some extend as there is a ``Fairy`` type Eeveelution (which sort of disproves that ``Dragon`` is a special type as there still isn't an Eeveelution of this type).
- The "physical" types are ordered according to the historical relevance as starter secondary types. For example, ``Poison`` was the secondary type of the base form of the initial three starters when both other starters didn't have a secondary type, so ``Poison`` goes first, and ``Flying`` is second.  The two types ``Bug`` and ``Rock`` were't yet used as starter secondary types and are ordered according to their relative abundance overall. Hence, these two could switch positions in the future, if the rarer type of the two becomes a starter secondary type first.
- The "special" types are ordered according to (a) Dex ordering of starter types (``Grass``, ``Fire``, ``Water``, ``Electric`` (if you count ``Pikachu`` as a starter, but even if not the position won't change, given the secondary ordering criterion listed next)) and (b) order in which Eeveelutions got introduced (``Water``, ``Electric``, ``Fire`` (which are already ordered according to (a)), ``Psychic`` (``Dark`` is not in generation 1 and will not follow the ordering), ``Ice`` (``Grass`` is already ordered previously), ``Fairy``).
- The ``Normal`` type is excluded from all other reasoning and is put in front of the list as a sort of root type, even if this concept of a root type is not applicable in the literal sense.

## Effectiveness indication

Normally, the effectiveness of types against each other is represented by factors. The neutral factor is ``1``, immunities are represented by the factor ``0``, "beneficial" factors are positive integers and "hindering" factors are fractions below ``1``. In the core games, all these factors are powers to the base ``2``. Except for ``0`` which can't be represented as the power to any base.

But calculating with fractions isn't really performant and can result in imprecise values (although calculations of powers to base ``2`` in the range that I handle them should actually be exact). Or in other words, I avoid floating point numbers where possible. So, I decided to represent these factors by their exponent alone to base ``2``. The neutral factor then is represented by ``0``, "beneficial" factors are positive integers and "hindering" factors are negative. To represent an immunity, I decided to use ``-10``. This is fine as long as you never use these factors for actual damage calculation, but even if you did, an immunity would result in a thousandth of the base damage, which isn't correct I admit, but close enough to not pose any problems. And only if Pokemon had eleven types, my representation of immunities could be problematic as this would be necessary to "balance out" the immunity and appearing as a normal effectiveness.

Instead of multiplying effectiveness factors, we just go ahead and add up any of our factors in case of dual types (which is mathematically sound, as multiplication is equivalent to addition on a logarithmical scale).

This though, can still mess up some comparisons. If a dual type combines an immunity with a resistance, the overall effectiveness is ``-11``, if instead an immunity is combined with a weakness, the result is ``-9``. Mathematically there is no problem here, as both results are far from a normal resistance, but if you were to sort by some defensive dmamage evaluation, these two would be handled differently. Therefore, I capped the damage calculation that a type can report to the range of ``[-7, 7]``.

## Defensive types

A type (as in the typing of a Pokemon) in this project is represented by its defensive properties. Hence, types are lists of factors. If you want to know how effective another type is against the given type you look up the value at the position that corresponds to the id associated with the attacking type (although this is abstracted away by some method).

Types have extension methods to assist in handling them and detecting/extracting specific properties.

## Type Combinations

A type combination is represented by the types it consists of and the defensive properties of the combination, hence, their combined type.

Type combinations have extension methods to assist in handling them and detecting/extracting specific properties.

## Type Matrices

A type matrix is a way to represent the type system of a game generation. Technically it's a two-dimensional matrix that stores how effective each type is against any other.

Type matrices have extension methods that extract any specific type combinations as well as providing lists of all combinations of some sort.

## Abilities

Abilities for the most part are not of any interest for this project, as most battle details aren't relevant for some type optimization. But a few abilities are able to influence type effectiveness. So, I added abilities as a concept to on-the-fly change the defensive properties of a type combination. This is done in form of extension methods on type combinations. Or, alternatively, as a mapping from ability names to their extension method, in case you want to parse abilities from a file.