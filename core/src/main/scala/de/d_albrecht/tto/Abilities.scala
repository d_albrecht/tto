/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto

import Definitions._

/**
  * This package offers convenience extension methods to apply the effects of some abilities to a type combination.
  * Some effects should be used with care as their representation in this format isn't completely faithful in some aspects.
  * Any abilities that rely on information other than type (Soundproof for sound based attacks,
  * abilities that distinguish physical and special attacks, contact based effects, ...) are not modeled in these extension methods at all.
  * Link: https://bulbapedia.bulbagarden.net/wiki/Category:Abilities_that_alter_damage_taken
  */
object Abilities {
	extension (combination: TypeCombination) {
		/* gen 3+ */
		def FlashFire: TypeCombination = combination.setFactor("Fire".typeId, -10)
		def Levitate: TypeCombination = combination.setFactor("Ground".typeId, -10)
		def ThickFat: TypeCombination = combination.applyFactor("Fire".typeId, -1).applyFactor("Ice".typeId, -1)
		def VoltAbsorb: TypeCombination = combination.setFactor("Electric".typeId, -10)
		def WaterAbsorb: TypeCombination = combination.setFactor("Water".typeId, -10)
		def WonderGuard: TypeCombination = combination.transformFactors((f) => if (f < 1) then -10 else f)
		/* gen 4+ */
		def DrySkin: TypeCombination = combination.setFactor("Water".typeId, -10)
		/** Filter reduces super effective damage by 25%, this is an approximation. */
		def Filter: TypeCombination = combination.transformFactors((f) => if (f > 1) then f - 1 else f)
		def Heatproof: TypeCombination = combination.applyFactor("Fire".typeId, -1)
		def MotorDrive: TypeCombination = combination.setFactor("Electric".typeId, -10)
		/** Solid Rock reduces super effective damage by 25%, this is an approximation. */
		def SolidRock: TypeCombination = combination.transformFactors((f) => if (f > 1) then f - 1 else f)
		/* gen 5+ */
		/** only granted immunity starting from gen 5, although the ability itself is older. */
		def LightningRod: TypeCombination = combination.setFactor("Electric".typeId, -10)
		/** Multiscale only applies at full health. Consider this before applying this ability. */
		def Multiscale: TypeCombination = combination.transformFactors((f) => f - 1)
		def SapSipper: TypeCombination = combination.setFactor("Grass".typeId, -10)
		/** only granted immunity starting from gen 5, although the ability itself is older. */
		def StormDrain: TypeCombination = combination.setFactor("Water".typeId, -10)
		/* gen 6+ */
		/* gen 7+ */
		def Fluffy: TypeCombination = combination.applyFactor("Fire".typeId, 1)
		/** Prism Armor reduces super effective damage by 25%, this is an approximation. */
		def PrismArmor: TypeCombination = combination.transformFactors((f) => if (f > 1) then f - 1 else f)
		/** Shadow Shield only applies at full health. Consider this before applying this ability. */
		def ShadowShield: TypeCombination = combination.transformFactors((f) => f - 1)
		/** This doesn't include that Water Bubble also doubles the damage of Water type attacks. */
		def WaterBubble: TypeCombination = combination.applyFactor("Fire".typeId, -1)
		/* gen 8+ */
		/* gen 9+ */
		def EarthEater: TypeCombination = combination.setFactor("Ground".typeId, -10)
		def PurifyingSalt: TypeCombination = combination.applyFactor("Ghost".typeId, -1)
		def WellBakedBody: TypeCombination = combination.setFactor("Fire".typeId, -10)
	}

	val abilityMap: Map[String, (TypeCombination) => TypeCombination] = Map(
		"Dry Skin" -> DrySkin, "DrySkin" -> DrySkin,
		"Earth Eater" -> EarthEater, "EarthEater" -> EarthEater,
		"Filter" -> Filter,
		"Flash Fire" -> FlashFire, "FlashFire" -> FlashFire,
		"Fluffy" -> Fluffy,
		"Heatproof" -> Heatproof,
		"Levitate" -> Levitate,
		"Lightning Rod" -> LightningRod, "LightningRod" -> LightningRod,
		"Motor Drive" -> MotorDrive, "MotorDrive" -> MotorDrive,
		"Multiscale" -> Multiscale,
		"Prism Armor" -> PrismArmor, "PrismArmor" -> PrismArmor,
		"Purifying Salt" -> PurifyingSalt, "PurifyingSalt" -> PurifyingSalt,
		"Sap Sipper" -> SapSipper, "SapSipper" -> SapSipper,
		"Shadow Shield" -> ShadowShield, "ShadowShield" -> ShadowShield,
		"Solid Rock" -> SolidRock, "SolidRock" -> SolidRock,
		"Storm Drain" -> StormDrain, "StormDrain" -> StormDrain,
		"Thick Fat" -> ThickFat, "ThickFat" -> ThickFat,
		"Volt Absorb" -> VoltAbsorb, "VoltAbsorb" -> VoltAbsorb,
		"Water Absorb" -> WaterAbsorb, "WaterAbsorb" -> WaterAbsorb,
		"Water Bubble" -> WaterBubble, "WaterBubble" -> WaterBubble,
		"Well-Baked Body" -> WellBakedBody, "Well Baked Body" -> WellBakedBody, "WellBakedBody" -> WellBakedBody,
		"Wonder Guard" -> WonderGuard, "WonderGuard" -> WonderGuard,
		"" -> ((c: TypeCombination) => c)
	)
}