/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto

object Definitions {
	private val typeNames: List[(Int, String)] = List(
		(0, "Normal"), (1, "Poison"), (2, "Flying"), (3, "Fighting"), (4, "Ground"), (5, "Ghost"), (6, "Bug"), (7, "Rock"),
		(8, "Grass"), (9, "Fire"), (10, "Water"), (11, "Electric"), (12, "Psychic"), (13, "Ice"), (14, "Dragon"),
		(15, "Steel"), (16, "Dark"), (17, "Fairy")
	)

	opaque type PkmnType = Int
	trait PkmnTypeOrdering extends Ordering[PkmnType] {
		def compare(x: PkmnType, y: PkmnType): Int = x.compare(y)
	}
	implicit object PkmnType extends PkmnTypeOrdering

	private lazy val idMap: Map[Int, String] = typeNames.toMap.withDefault(_ => "invalid")
	extension (typeId: PkmnType) {
		def typeString: String = idMap(typeId)
		def asInt: Int = typeId
		def valid: Boolean = typeId > -1
	}

	private lazy val nameMap: Map[String, Int] = typeNames.map(_.swap).toMap.withDefault(_ => -1)
	extension (typeString: String) {
		def typeId: PkmnType = nameMap(typeString)
	}
	extension (combinationStrings: (String, String)) {
		def typeIds: (PkmnType, PkmnType) = (combinationStrings._1.typeId, combinationStrings._2.typeId)
	}

	opaque type TypeMatrix = List[List[Int]]
	extension (matrix: TypeMatrix) {
		def combination(ids: PkmnType*): TypeEffectiveness = ids.map(matrix(_)).reduce((a, b) => a.zip(b).map(e => e._1 + e._2))
		def combination(ids: (PkmnType, PkmnType)): TypeCombination = if (ids._1 == ids._2) then (ids, combination(ids._1)) else (ids, combination(ids._1, ids._2))
		def allTypes: List[PkmnType] = (0 until matrix.size).toList
		def allUniqueCombinations: List[TypeCombination] = (for (t1 <- allTypes; t2 <- t1 until matrix.size) yield combination((t1, t2))).toList
		def allCombinations: List[TypeCombination] = (for (t1 <- allTypes; t2 <- allTypes) yield combination((t1, t2))).toList
		def allMonoTypes: List[TypeCombination] = (for (t <- allTypes) yield combination((t, t))).toList
		def allUniqueDualTypes: List[TypeCombination] = (for (t1 <- allTypes; t2 <- t1 + 1 until matrix.size) yield combination((t1, t2))).toList
		def allDualTypes: List[TypeCombination] = (for (t1 <- allTypes; t2 <- allTypes; if t1 != t2) yield combination((t1, t2))).toList
	}
	extension (matrix: List[List[Int]]) {
		def asMatrix(): TypeMatrix = {
			val lineWidth = matrix.map(_.size).distinct
			if (lineWidth.size != 1 || lineWidth.head != matrix.size) {
				throw new IllegalArgumentException("Given matrix isn't complete")
			}
			matrix
		}
	}

	opaque type TypeEffectiveness = List[Int]
	extension (effectiveness: TypeEffectiveness) {
		def noWeaknesses: Boolean = effectiveness.forall(e => e <= 0)
		def uniqueWeakness: Boolean = effectiveness.count(e => e > 0) == 1
		def fourTimesWeakness: Boolean = effectiveness.exists(e => e > 1)
		def getWeaknesses: List[PkmnType] = effectiveness.zipWithIndex.filter(e => e._1 > 0).map(_._2)
		def getResistances: List[PkmnType] = effectiveness.zipWithIndex.filter(e => e._1 < 0).map(_._2)
		/**
		  * Evaluates the given attacks and returns the degree of effectiveness of the most effective move among these types.
		  * Positive return values denote super effective attacks, negative values not very effective attacks.
		  * The evaluation is capped to the range of -7 to 7 to standardize immunities.
		  */
		def evaluateAttack(types: PkmnType*): Int = types.map(effectiveness.apply).reduceOption(Math.max).map(e => Math.max(Math.min(e, 7), -7)).getOrElse(-7)
	}

	opaque type TypeCombination = ((PkmnType, PkmnType), TypeEffectiveness)
	extension (combination: TypeCombination) {
		def comb: (PkmnType, PkmnType) = combination._1
		def effectiveness: TypeEffectiveness = combination._2
		def selfWeakness: Boolean = !effectiveness.getWeaknesses.intersect(comb.toList).isEmpty
		def asBattler: Battler = (combination, comb.toList.distinct)
		def asBattler(attacks: List[PkmnType]): Battler = (combination, attacks)
		def asBattler(name: String): NamedBattler = (name, (combination, comb.toList.distinct))
		def asBattler(name: String, attacks: List[PkmnType]): NamedBattler = (name, (combination, attacks))
		def transformFactors(f: Int => Int): TypeCombination = (comb, effectiveness.map(f))
		def applyFactor(id: PkmnType, factor: Int): TypeCombination = setFactor(id, effectiveness(id) + factor)
		def setFactor(id: PkmnType, factor: Int): TypeCombination = (comb, effectiveness.updated(id, factor))
		def readable: (String, String) = (comb._1.typeString, if (comb._1 == comb._2) then "Mono" else comb._2.typeString)
	}

	case class MatchupEvaluation(defensive: Int, offensive: Int) {
		def defFirst: (Int, Int) = (defensive, offensive)
		def offFirst: (Int, Int) = (offensive, defensive)
		def min: Int = Math.min(defensive, offensive)
		def max: Int = Math.max(defensive, offensive)
		def sorted: (Int, Int) = (min, max)
		def sum: Int = defensive + offensive
		override def toString(): String = s"(def: $defensive, off: $offensive)"
	}

	opaque type Battler = (TypeCombination, List[PkmnType])
	extension (battler: Battler) {
		def withName(name: String): NamedBattler = (name, battler)
		def defense: TypeCombination = battler._1
		def attacks: List[PkmnType] = battler._2
		/**
		  * Evaluates the quality of a matchup between the two battler.
		  * Positive judgements means that the base battler is superior to the argument battler.
		  * @param matchup The opponent to evaluate against.
		  * @return The evaluation of the matchup.
		  */
		def evaluateMatchup(counter: Battler): MatchupEvaluation = {
			MatchupEvaluation(-battler.defense.effectiveness.evaluateAttack(counter.attacks: _*), counter.defense.effectiveness.evaluateAttack(battler.attacks: _*))
		}
	}

	opaque type NamedBattler = (String, Battler)
	extension (battler: NamedBattler) {
		def name: String = battler._1
		def battler: Battler = battler._2
	}
}