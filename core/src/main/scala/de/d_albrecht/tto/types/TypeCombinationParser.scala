/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package types

import Abilities._
import Definitions._

case class ParserParameters(parseAbility: Boolean = true, parseAttacks: Boolean = true)
case class ParserResult(name: String, comb: (PkmnType, PkmnType), ability: String, attacks: List[PkmnType]) {
	def name_= (newName: String): ParserResult = ParserResult(newName, comb, ability, attacks)
	def comb_= (newComb: (PkmnType, PkmnType)): ParserResult = ParserResult(name, newComb, ability, attacks)
	def ability_= (newAbility: String): ParserResult = ParserResult(name, comb, newAbility, attacks)
	def attacks_= (newAttacks: List[PkmnType]): ParserResult = ParserResult(name, comb, ability, newAttacks)
	def asBattler(matrix: TypeMatrix): Battler = {
		abilityMap(ability)(matrix.combination(comb)).asBattler(attacks)
	}
	def asNamedBattler(matrix: TypeMatrix): NamedBattler = {
		abilityMap(ability)(matrix.combination(comb)).asBattler(name, attacks)
	}
	def withStab(): ParserResult = {
		attacks = (comb.toList ::: attacks).distinct
	}
}

/**
  * The parser assumes the following format:
  * - Each line represents one Pokemon / type combination
  * - Each line is split into several segments with semicolon (";")
  * - The initial segment of each line is an identifier of some sort, for example for a name. Some clients of this base parser might add specific specifications to how this column is to be used. If it can be left empty, these lines have to start with a semicolon.
  * - The second segment describes the type combination.
  *   - For mono types this segment only contains the string name of the type (e.g. "Flying") with first character upper case, the rest lower case
  *   - For dual types, this segment contains two type names in the same format, separated by a comma (",")
  * - If enabled, the third segment denotes the ability. As the parser isn't able to verify every ability in existance, this segment adheres to the following principle:
  *   - For Pokemon without notable ability (anything that doesn't change the type effectiveness), this segment should be left blank
  *   - For any ability that alters the type effectiveness, this segment should contain the name of this ability in usual way of writing.
  * - If parsing attacks as well, the fourth segment is a list of type names separated by comma (",") like the dual type, but can contain any number of type names.
  * - Any segments after the required one(s) are free to use for any purpose and can contain any number of semicolon as well.
  * - To make it possible to verify the parser results, each line that can't be "fully" parsed, is dropped. Hence, if the number of filled lines in the input doesn't match the parsed combinations, then there probably was a typo in the input file.
  */
object TypeCombinationParser {
	def parseCombination(segment: String): Option[(PkmnType, PkmnType)] = {
		val types = segment.split(",").map(_.trim().typeId).toList
		types match {
			case mono :: Nil if mono.valid => Some(mono, mono)
			case prim :: sec :: Nil if prim.valid && sec.valid => Some(prim, sec)
			case _ => None
		}
	}
	def parseAbility(segment: String): Option[String] = {
		if (abilityMap.contains(segment.trim())) then Some(segment.trim()) else None
	}
	def parseAttacks(segment: String): Option[List[PkmnType]] = {
		if (segment.isBlank()) {
			Some(Nil)
		} else {
			val attacks = segment.split(",").filterNot(_.isBlank()).map(_.trim()).toList.map(_.typeId)
			if (attacks.exists(!_.valid)) then None else Some(attacks)
		}
	}
	def parseLine(line: String, params: ParserParameters = ParserParameters()): Option[ParserResult] = {
		val segments = line.split(";").toList
		val comb: Option[(PkmnType, PkmnType)] = segments.unapply(1).flatMap(parseCombination)
		val ability: Option[String] = if (params.parseAbility) then
			parseAbility(segments.applyOrElse(2, _ => "")) else Some("")
		val attacks: Option[List[PkmnType]] = if (params.parseAttacks) then
			parseAttacks(segments.applyOrElse(3, _ => "")) else Some(Nil)
		(comb, ability, attacks) match {
			case (Some(c), Some(ab), Some(at)) => Some(ParserResult(segments(0).trim(), c, ab, at))
			case _ => None
		}
	}
	def parseLines(lines: List[String], params: ParserParameters = ParserParameters()): List[ParserResult] = {
		lines.flatMap(l => parseLine(l, params))
	}
}