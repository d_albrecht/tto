/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package types

import Definitions._

/**
  * Defines type matrices for the existing generations of pokemon games.
  */
object TypeMatrices {
	/**
	  * The type matrix for generation 1.
	  */
	def gen1: TypeMatrix = List(
		//Atk: N, Po, Fl, Ft, Gd, Gh,  B,  R, Gr, Fi,  W,  E, Ps,  I, Dr
		List(  0,  0,  0,  1,  0,-10,  0,  0,  0,  0,  0,  0,  0,  0,  0),// N
		List(  0, -1,  0, -1,  1,  0,  1,  0, -1,  0,  0,  0,  1,  0,  0),// Po
		List(  0,  0,  0, -1,-10,  0, -1,  1,  0,  0,  0,  1,  0,  1,  0),// Fl
		List(  0,  0,  1,  0,  0,  0, -1, -1,  0,  0,  0,  0,  1,  0,  0),// Ft
		List(  0, -1,  0,  0,  0,  0,  0, -1,  1,  0,  1,-10,  0,  1,  0),// Gd
		List(-10, -1,  0,-10,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  0),// Gh
		List(  0,  1,  1, -1, -1,  0,  0,  1, -1,  1,  0,  0,  0,  0,  0),// B
		List( -1, -1, -1,  1,  1,  0,  0,  0,  1, -1,  1,  0,  0,  0,  0),// R
		List(  0,  1,  1,  0, -1,  0,  1,  0, -1,  1, -1, -1,  0,  1,  0),// Gr
		List(  0,  0,  0,  0,  1,  0, -1,  1, -1, -1,  1,  0,  0,  0,  0),// Fi
		List(  0,  0,  0,  0,  0,  0,  0,  0,  1, -1, -1,  1,  0, -1,  0),// W
		List(  0,  0, -1,  0,  1,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0),// E
		List(  0,  0,  0, -1,  0,-10,  1,  0,  0,  0,  0,  0, -1,  0,  0),// Ps
		List(  0,  0,  0,  1,  0,  0,  0,  1,  0,  1,  0,  0,  0, -1,  0),// I
		List(  0,  0,  0,  0,  0,  0,  0,  0, -1, -1, -1, -1,  0,  1,  1)// Dr
	).asMatrix()
	/**
	  * The type matrix for the generations 2 to 5
	  */
	def gen2: TypeMatrix = gen6Raw.take(17).map(_.take(17)).updated("Steel".typeId.asInt, steelGen2).asMatrix()
	private val steelGen2: List[Int] =
		List( -1,-10, -1,  1,  1, -1, -1, -1, -1,  1,  0,  0, -1, -1, -1, -1, -1)
	/**
	  * The type matrix for generations 6 and later
	  */
	def gen6: TypeMatrix = gen6Raw.asMatrix()
	private val gen6Raw: List[List[Int]] = List(
		//Atk: N, Po, Fl, Ft, Gd, Gh,  B,  R, Gr, Fi,  W,  E, Ps,  I, Dr,  S, Dk, Fy
		List(  0,  0,  0,  1,  0,-10,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0),// N
		List(  0, -1,  0, -1,  1,  0, -1,  0, -1,  0,  0,  0,  1,  0,  0,  0,  0, -1),// Po
		List(  0,  0,  0, -1,-10,  0, -1,  1,  0,  0,  0,  1,  0,  1,  0,  0,  0,  0),// Fl
		List(  0,  0,  1,  0,  0,  0, -1, -1,  0,  0,  0,  0,  1,  0,  0,  0, -1,  1),// Ft
		List(  0, -1,  0,  0,  0,  0,  0, -1,  1,  0,  1,-10,  0,  1,  0,  0,  0,  0),// Gd
		List(-10, -1,  0,-10,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0),// Gh
		List(  0,  0,  1, -1, -1,  0,  0,  1, -1,  1,  0,  0,  0,  0,  0,  0,  0,  0),// B
		List( -1, -1, -1,  1,  1,  0,  0,  0,  1, -1,  1,  0,  0,  0,  0,  1,  0,  0),// R
		List(  0,  1,  1,  0, -1,  0,  1,  0, -1,  1, -1, -1,  0,  1,  0,  0,  0,  0),// Gr
		List(  0,  0,  0,  0,  1,  0, -1,  1, -1, -1,  1,  0,  0, -1,  0, -1,  0, -1),// Fi
		List(  0,  0,  0,  0,  0,  0,  0,  0,  1, -1, -1,  1,  0, -1,  0, -1,  0,  0),// W
		List(  0,  0, -1,  0,  1,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0, -1,  0,  0),// E
		List(  0,  0,  0, -1,  0,  1,  1,  0,  0,  0,  0,  0, -1,  0,  0,  0,  1,  0),// Ps
		List(  0,  0,  0,  1,  0,  0,  0,  1,  0,  1,  0,  0,  0, -1,  0,  1,  0,  0),// I
		List(  0,  0,  0,  0,  0,  0,  0,  0, -1, -1, -1, -1,  0,  1,  1,  0,  0,  1),// Dr
		List( -1,-10, -1,  1,  1,  0, -1, -1, -1,  1,  0,  0, -1, -1, -1, -1,  0, -1),// S
		List(  0,  0,  0,  1,  0, -1,  1,  0,  0,  0,  0,  0,-10,  0,  0,  0, -1,  1),// Dk
		List(  0,  1,  0, -1,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,-10,  1, -1,  0))// Fy
}