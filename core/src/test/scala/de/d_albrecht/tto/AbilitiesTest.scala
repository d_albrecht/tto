/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto

import Definitions._

import types._

import DefinitionsFixture._
import TestCombinations._

class AbilitiesTest extends TestBase {
	"Abilities" should {
		"correctly apply ability" in {
			val matrix = TypeMatrices.gen6
			val name = "Wonder Guard"
			val combination = matrix.combination(SHEDINJA)
			val expected = (("Bug", "Ghost").typeIds, List(-10, -10, 1, -10, -10, 1, -10, 1, -10, 1, -10, -10, -10, -10, -10, -10, 1, -10))

			val uut = Abilities

			uut.WonderGuard(combination) should be (expected)
			uut.abilityMap(name)(combination) should be (expected)
		}

		"apply Filter-like abilities" in {
			val combination = asTypeCombination((0, 0), List(-3, -2, -1, 0, 1, 2, 3))
			val expected = asTypeCombination((0, 0), List(-3, -2, -1, 0, 1, 1, 2))

			val uut = Abilities

			uut.Filter(combination) should be (expected)
			uut.abilityMap("Filter")(combination) should be (expected)
			uut.SolidRock(combination) should be (expected)
			uut.abilityMap("SolidRock")(combination) should be (expected)
			uut.abilityMap("Solid Rock")(combination) should be (expected)
			uut.PrismArmor(combination) should be (expected)
			uut.abilityMap("PrismArmor")(combination) should be (expected)
			uut.abilityMap("Prism Armor")(combination) should be (expected)
		}

		"correctly apply Flash Fire" in {
			val matrix = TypeMatrices.gen6
			val uniqueWeakness = matrix.combination(SCIZOR)

			val uut = Abilities

			uniqueWeakness.effectiveness.getWeaknesses should be (List("Fire".typeId))
			uut.FlashFire(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.abilityMap("FlashFire")(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.abilityMap("Flash Fire")(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.WellBakedBody(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.abilityMap("WellBakedBody")(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.abilityMap("Well Baked Body")(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.abilityMap("Well-Baked Body")(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
		}

		"correctly apply Levitate" in {
			val matrix = TypeMatrices.gen6
			val uniqueWeakness = matrix.combination(JOLTEON)

			val uut = Abilities

			uniqueWeakness.effectiveness.getWeaknesses should be (List("Ground".typeId))
			uut.Levitate(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.abilityMap("Levitate")(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.EarthEater(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.abilityMap("EarthEater")(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.abilityMap("Earth Eater")(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
		}

		"correctly apply Thick Fat" in {
			val matrix = TypeMatrices.gen6
			val limitedWeaknesses = matrix.combination(ZAPDOS)
			val expectedWeaknesses = List("Rock".typeId)

			val uut = Abilities

			limitedWeaknesses.effectiveness.getWeaknesses should be (List("Rock", "Ice").map(_.typeId))
			uut.ThickFat(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("ThickFat")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("Thick Fat")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
		}

		"correctly apply Volt Absorb" in {
			val matrix = TypeMatrices.gen6
			val limitedWeaknesses = matrix.combination(VAPOREON)
			val expectedWeaknesses = List("Grass".typeId)

			val uut = Abilities

			limitedWeaknesses.effectiveness.getWeaknesses should be (List("Grass", "Electric").map(_.typeId))
			uut.VoltAbsorb(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("VoltAbsorb")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("Volt Absorb")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.MotorDrive(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("MotorDrive")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("Motor Drive")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.LightningRod(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("LightningRod")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("Lightning Rod")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
		}

		"correctly apply Water Absorb" in {
			val matrix = TypeMatrices.gen6
			val limitedWeaknesses = matrix.combination(CAMERUPT)
			val expectedWeaknesses = List("Ground".typeId)

			val uut = Abilities

			limitedWeaknesses.effectiveness.getWeaknesses should be (List("Ground", "Water").map(_.typeId))
			uut.WaterAbsorb(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("WaterAbsorb")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("Water Absorb")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.DrySkin(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("DrySkin")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("Dry Skin")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.StormDrain(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("StormDrain")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("Storm Drain")(limitedWeaknesses).effectiveness.getWeaknesses should be (expectedWeaknesses)
		}

		"correctly apply Wonder Guard" in {
			val matrix = TypeMatrices.gen2
			val noWeakness = matrix.combination(SABLEYE)

			val uut = Abilities

			noWeakness.effectiveness.evaluateAttack(matrix.allTypes: _*) should be (0)
			uut.WonderGuard(noWeakness).effectiveness.evaluateAttack(matrix.allTypes: _*) should be (-7)
			uut.abilityMap("WonderGuard")(noWeakness).effectiveness.evaluateAttack(matrix.allTypes: _*) should be (-7)
			uut.abilityMap("Wonder Guard")(noWeakness).effectiveness.evaluateAttack(matrix.allTypes: _*) should be (-7)
		}

		"correctly apply Heatproof" in {
			val matrix = TypeMatrices.gen6
			val limitedResistances = matrix.combination(MAWILE)
			val expectedWeaknesses = List("Ground".typeId)

			val uut = Abilities

			limitedResistances.effectiveness.getWeaknesses should be (List("Ground", "Fire").map(_.typeId))
			uut.Heatproof(limitedResistances).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("Heatproof")(limitedResistances).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.WaterBubble(limitedResistances).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("WaterBubble")(limitedResistances).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("Water Bubble")(limitedResistances).effectiveness.getWeaknesses should be (expectedWeaknesses)
		}

		"correctly apply Multiscale" in {
			val matrix = TypeMatrices.gen6
			val limitedResistances = matrix.combination(CAMERUPT)
			val expectedWeaknesses = List("Water".typeId)

			val uut = Abilities

			def isCorrect(comb: TypeCombination): Unit = {
				comb.effectiveness.getWeaknesses should be (expectedWeaknesses)
				comb.effectiveness.evaluateAttack("Ground".typeId) should be (0)
				comb.effectiveness.evaluateAttack("Water".typeId) should be (1)
			}

			limitedResistances.effectiveness.getWeaknesses should be (List("Ground", "Water").map(_.typeId))
			limitedResistances.effectiveness.evaluateAttack("Ground".typeId) should be (1)
			limitedResistances.effectiveness.evaluateAttack("Water".typeId) should be (2)
			isCorrect(uut.Multiscale(limitedResistances))
			isCorrect(uut.abilityMap("Multiscale")(limitedResistances))
			isCorrect(uut.ShadowShield(limitedResistances))
			isCorrect(uut.abilityMap("ShadowShield")(limitedResistances))
			isCorrect(uut.abilityMap("Shadow Shield")(limitedResistances))
		}

		"correctly apply Sap Sipper" in {
			val matrix = TypeMatrices.gen6
			val uniqueWeakness = matrix.combination(GASTRODON)

			val uut = Abilities

			uniqueWeakness.effectiveness.getWeaknesses should be (List("Grass".typeId))
			uut.SapSipper(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.abilityMap("SapSipper")(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
			uut.abilityMap("Sap Sipper")(uniqueWeakness).effectiveness.getWeaknesses should be (Nil)
		}

		"correctly apply Fluffy" in {
			val matrix = TypeMatrices.gen6
			val limitedResistances = matrix.combination(GARCHOMP)
			val expectedResistances = List("Poison", "Rock", "Electric").map(_.typeId)

			val uut = Abilities

			limitedResistances.effectiveness.getResistances should be (List("Poison", "Rock", "Fire", "Electric").map(_.typeId))
			uut.Fluffy(limitedResistances).effectiveness.getResistances should be (expectedResistances)
			uut.abilityMap("Fluffy")(limitedResistances).effectiveness.getResistances should be (expectedResistances)
		}

		"correctly apply Purifying Salt" in {
			val matrix = TypeMatrices.gen6
			val limitedResistances = matrix.combination(MIMIKYU)
			val expectedWeaknesses = List("Steel".typeId)

			val uut = Abilities

			limitedResistances.effectiveness.getWeaknesses should be (List("Ghost", "Steel").map(_.typeId))
			uut.PurifyingSalt(limitedResistances).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("PurifyingSalt")(limitedResistances).effectiveness.getWeaknesses should be (expectedWeaknesses)
			uut.abilityMap("Purifying Salt")(limitedResistances).effectiveness.getWeaknesses should be (expectedWeaknesses)
		}
	}
}
