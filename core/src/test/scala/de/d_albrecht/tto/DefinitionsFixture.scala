/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto

import Definitions._

object DefinitionsFixture {
	def asType(t: Int): PkmnType = {
		t.asInstanceOf[PkmnType]
	}

	def asCombination(c: (Int, Int)): (PkmnType, PkmnType) = {
		(asType(c._1), asType(c._2))
	}

	def asEffectiveness(e: List[Int]): TypeEffectiveness = {
		List.fill(e.size)(e).asMatrix().combination(asType(0))
	}

	def asTypeCombination(comb: (Int, Int), l: List[Int]): TypeCombination = {
		List.fill(l.size)(List.fill(l.size)(0)).patch(comb._1, List(l), 1).asMatrix().combination(asCombination(comb))
	}

	def asTestBattler(comb: (Int, Int), l: List[Int], attacks: List[Int]): Battler = {
		asTypeCombination(comb, l).asBattler(l.map(asType))
	}
}
