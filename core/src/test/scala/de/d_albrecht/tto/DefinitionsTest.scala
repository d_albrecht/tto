/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto

import Definitions._

import types._

import DefinitionsFixture._
import TestCombinations._

class DefinitionsTest extends TestBase {
	"TypeOrdering" should {
		"order types" in {
			val types = List("Bug", "Dark", "Fairy", "Ice", "Poison").map(_.typeId)
			val expected = List("Poison", "Bug", "Ice", "Dark", "Fairy")

			types.sorted.map(_.typeString) should be (expected)
		}
	}

	"Type Conversions" should {
		"be reversible" in {
			val typeString = "Bug"

			typeString.typeId.typeString should be (typeString)
		}

		"not be reversible" in {
			val typeString = "Metal"

			typeString.typeId.typeString should be ("invalid")
		}

		"flag valid strings" in {
			val typeString = "Bug"

			typeString.typeId.valid should be (true)
		}

		"flag invalid strings" in {
			val typeString = "Metal"

			typeString.typeId.valid should be (false)
		}

		"handle tuples" in {
			val type1 = "Bug"
			val type2 = "Steel"
			val expected = (type1.typeId, type2.typeId)

			(type1, type2).typeIds should be (expected)
		}

		"offer numerical types" in {
			val typeString = "Normal"

			typeString.typeId.asInt should be (0)
		}
	}

	"Type Matrix" should {
		"fail on ill-shaped matrices" in {
			assertThrows[IllegalArgumentException] {
				val pseudoMatrix = List(List(0), List(1, 2), List(3, 4, 5))

				pseudoMatrix.asMatrix()
			}
		}

		"be converted without changes" in {
			val matrix = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))

			matrix.asMatrix() should be (matrix)
		}

		"fail on invalid type" in {
			assertThrows[IndexOutOfBoundsException] {
				val matrix = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))

				matrix.asMatrix().combination("invalid".typeId)
			}
		}

		"return first line" in {
			val expected = List(1, 2, 3)
			val matrix = List(expected, List(4, 5, 6), List(7, 8, 9))

			matrix.asMatrix().combination(asType(0)) should be (expected)
		}

		"return first line as mono combination" in {
			val effectiveness = List(1, 2, 3)
			val matrix = List(effectiveness, List(4, 5, 6), List(7, 8, 9))
			val comb = (0, 0)

			matrix.asMatrix().combination(asCombination(comb)) should be ((comb, effectiveness))
		}

		"return combination" in {
			val matrix = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))
			val comb = (0, 1)
			val expected = (comb, List(5, 7, 9))

			matrix.asMatrix().combination(asCombination(comb)) should be (expected)
		}

		"return all types" in {
			val matrix = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))
			val expected = (0 until matrix.size).toList

			matrix.asMatrix().allTypes should be (expected)
		}

		"return all combinations" in {
			val matrix = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))
			val expected = Set((0, 0), (1, 1), (2, 2), (0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1))

			matrix.asMatrix().allCombinations.map(_.comb).toSet should be (expected)
		}

		"return all unique combinations" in {
			val matrix = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))
			val expected = Set((0, 0), (1, 1), (2, 2), (0, 1), (0, 2), (1, 2))

			matrix.asMatrix().allUniqueCombinations.map(_.comb).toSet should be (expected)
		}

		"return all mono types" in {
			val matrix = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))
			val expected = matrix.zipWithIndex.map{ case (l, i) => ((i, i), l)}

			matrix.asMatrix().allMonoTypes should be (expected)
		}

		"return all dual types" in {
			val matrix = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))
			val expected = Set((0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1))

			matrix.asMatrix().allDualTypes.map(_.comb).toSet should be (expected)
		}

		"return all unique dual types" in {
			val matrix = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))
			val expected = Set((0, 1), (0, 2), (1, 2))

			matrix.asMatrix().allUniqueDualTypes.map(_.comb).toSet should be (expected)
		}
	}

	"Type Effectiveness" should {
		"detect no weaknesses" in {
			val someType = asEffectiveness(List(-1, -2, -3, -4, -5))

			someType.noWeaknesses should be (true)
		}

		"detect weaknesses" in {
			val someType = asEffectiveness(List(-1, -2, 3, -4, -5))

			someType.noWeaknesses should be (false)
		}

		"detect no unique weaknesses" in {
			val someType = asEffectiveness(List(-1, 2, -3, 4, -5))

			someType.uniqueWeakness should be (false)
		}

		"detect unique weaknesses" in {
			val someType = asEffectiveness(List(-1, -2, 3, -4, -5))

			someType.uniqueWeakness should be (true)
		}

		"detect no four-times weaknesses" in {
			val someType = asEffectiveness(List(-1, 1, -3, 1, -5))

			someType.fourTimesWeakness should be (false)
		}

		"detect four-times weaknesses" in {
			val someType = asEffectiveness(List(-1, 2, -3, 4, -5))

			someType.fourTimesWeakness should be (true)
		}

		"return all weaknesses" in {
			val someType = asEffectiveness(List(-1, 2, -3, 4, -5))
			val expected = Set(1, 3)

			someType.getWeaknesses.toSet should be (expected)
		}

		"return all resistances" in {
			val someType = asEffectiveness(List(-1, 2, -3, 4, -5))
			val expected = Set(0, 2, 4)

			someType.getResistances.toSet should be (expected)
		}

		"evaluate attack" in {
			val someType = asEffectiveness(List(-1, 2, -3, 4, -5))
			val attacks = List(1, 2, 4).map(asType)

			someType.evaluateAttack(attacks: _*) should be (2)
		}

		"evaluate resisted attack" in {
			val someType = asEffectiveness(List(-1, 2, -3, 4, -5))
			val attacks = List(0, 2, 4).map(asType)

			someType.evaluateAttack(attacks: _*) should be (-1)
		}
	}

	"Type Combination" should {
		"return combination" in {
			val comb = (0, 1)
			val effectiveness = List(1, 2, 3)

			asTypeCombination(comb, effectiveness).comb should be (comb)
		}

		"return effectiveness" in {
			val comb = (0, 1)
			val effectiveness = List(1, 2, 3)

			asTypeCombination(comb, effectiveness).effectiveness should be (effectiveness)
		}

		"detect no self-weaknesses" in {
			val comb = (0, 1)
			val effectiveness = List(-1, -2, 3)

			asTypeCombination(comb, effectiveness).selfWeakness should be (false)
		}

		"detect self-weaknesses" in {
			val comb = (0, 1)
			val effectiveness = List(-1, 2, -3)

			asTypeCombination(comb, effectiveness).selfWeakness should be (true)
		}

		"return battler" in {
			val comb = (0, 1)
			val effectiveness = List(-1, 2, -3)
			val attacks = comb.toList.distinct

			asTypeCombination(comb, effectiveness).asBattler should be (((comb, effectiveness), attacks))
		}

		"return specific battler" in {
			val comb = (0, 1)
			val effectiveness = List(-1, 2, -3)
			val attacks = List(2).map(asType)

			asTypeCombination(comb, effectiveness).asBattler(attacks) should be (((comb, effectiveness), attacks))
		}

		"return named battler" in {
			val comb = (0, 1)
			val effectiveness = List(-1, 2, -3)
			val attacks = comb.toList.distinct
			val name = "Champ"

			asTypeCombination(comb, effectiveness).asBattler(name) should be (name, (((comb, effectiveness), attacks)))
		}

		"return specific named battler" in {
			val comb = (0, 1)
			val effectiveness = List(-1, 2, -3)
			val attacks = List(2).map(asType)
			val name = "Champ"

			asTypeCombination(comb, effectiveness).asBattler(name, attacks) should be (name, (((comb, effectiveness), attacks)))
		}

		"transform effectiveness factors" in {
			val comb = (0, 1)
			val effectiveness = List(-1, 2, -3)
			val f = (v: Int) => v
			val expected = List(-1, 2, -3)

			asTypeCombination(comb, effectiveness).transformFactors(f) should be (asTypeCombination(comb, expected))
		}

		"apply effectiveness factor" in {
			val comb = (0, 1)
			val effectiveness = List(-1, 2, -3)
			val alteredType = asType(2)
			val factor = 4
			val expected = List(-1, 2, 1)

			asTypeCombination(comb, effectiveness).applyFactor(alteredType, factor) should be (asTypeCombination(comb, expected))
		}

		"set effectiveness factor" in {
			val comb = (0, 1)
			val effectiveness = List(-1, 2, -3)
			val alteredType = asType(2)
			val newValue = 4
			val expected = List(-1, 2, 4)

			asTypeCombination(comb, effectiveness).setFactor(alteredType, newValue) should be (asTypeCombination(comb, expected))
		}

		"provide readable representation" in {
			val comb = (0, 1)
			val effectiveness = List(-1, 2, -3)
			val expected = (asType(comb._1).typeString, asType(comb._2).typeString)

			asTypeCombination(comb, effectiveness).readable should be (expected)
		}
	}

	"Battler" should {
		"evaluate matchup" in {
			val matrix = TypeMatrices.gen6
			val teamMember = matrix.combination(GOLEM).asBattler
			val opponent = matrix.combination(EEVEE).asBattler

			teamMember.evaluateMatchup(opponent) should be (MatchupEvaluation(1, 0))
		}

		"provide sorted matchup" in {
			val matrix = TypeMatrices.gen6
			val teamMember = matrix.combination(GOLEM).asBattler
			val opponent = matrix.combination(EEVEE).asBattler

			teamMember.evaluateMatchup(opponent).sorted should be ((0, 1))
		}

		"provide sorted matchup with extreme advantage" in {
			val matrix = TypeMatrices.gen6
			val teamMember = matrix.combination(HEATRAN).asBattler
			val opponent = matrix.combination(PARASECT).asBattler

			teamMember.evaluateMatchup(opponent).sorted should be ((2, 2))
		}

		"convert to named battler" in {
			val matrix = TypeMatrices.gen6
			val battler = matrix.combination(BULBASAUR).asBattler
			val name = "Bulbasaur"

			battler.withName(name) should be (name, battler)
		}
	}
}
