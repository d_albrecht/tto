/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto

import Definitions._

object TestCombinations {
	val BLASTOISE: (PkmnType, PkmnType) = ("Water", "Water").typeIds
	val BULBASAUR: (PkmnType, PkmnType) = ("Grass", "Poison").typeIds
	val CAMERUPT: (PkmnType, PkmnType) = ("Fire", "Ground").typeIds
	val CATERPIE: (PkmnType, PkmnType) = ("Bug", "Bug").typeIds
	val CHARIZARD: (PkmnType, PkmnType) = ("Fire", "Flying").typeIds
	val EEVEE: (PkmnType, PkmnType) = ("Normal", "Normal").typeIds
	val ESPEON: (PkmnType, PkmnType) = ("Psychic", "Psychic").typeIds
	val FLAREON: (PkmnType, PkmnType) = ("Fire", "Fire").typeIds
	val GARCHOMP: (PkmnType, PkmnType) = ("Dragon", "Ground").typeIds
	val GASTLY: (PkmnType, PkmnType) = ("Ghost", "Poison").typeIds
	val GASTRODON: (PkmnType, PkmnType) = ("Water", "Ground").typeIds
	val GLACEON: (PkmnType, PkmnType) = ("Ice", "Ice").typeIds
	val GOLEM: (PkmnType, PkmnType) = ("Rock", "Ground").typeIds
	val GYARADOS: (PkmnType, PkmnType) = ("Water", "Flying").typeIds
	val HEATRAN: (PkmnType, PkmnType) = ("Fire", "Steel").typeIds
	val JOLTEON: (PkmnType, PkmnType) = ("Electric", "Electric").typeIds
	val KABUTO: (PkmnType, PkmnType) = ("Rock", "Water").typeIds
	val KINGDRA: (PkmnType, PkmnType) = ("Water", "Dragon").typeIds
	val LEAFEON: (PkmnType, PkmnType) = ("Grass", "Grass").typeIds
	val LUCARIO: (PkmnType, PkmnType) = ("Fighting", "Steel").typeIds
	val MAGIKARP: (PkmnType, PkmnType) = ("Water", "Water").typeIds
	val MAGNEMITE: (PkmnType, PkmnType) = ("Electric", "Steel").typeIds
	val MAWILE: (PkmnType, PkmnType) = ("Steel", "Fairy").typeIds
	val MILOTIC: (PkmnType, PkmnType) = ("Water", "Water").typeIds
	val MIMIKYU: (PkmnType, PkmnType) = ("Ghost", "Fairy").typeIds
	val MURKROW: (PkmnType, PkmnType) = ("Dark", "Flying").typeIds
	val PARASECT: (PkmnType, PkmnType) = ("Bug", "Grass").typeIds
	val PAWNIARD: (PkmnType, PkmnType) = ("Dark", "Steel").typeIds
	val PIDGEY: (PkmnType, PkmnType) = ("Normal", "Flying").typeIds
	val PIKACHU: (PkmnType, PkmnType) = ("Electric", "Electric").typeIds
	val RAICHU: (PkmnType, PkmnType) = ("Electric", "Electric").typeIds
	val RAYQUAZA: (PkmnType, PkmnType) = ("Dragon", "Flying").typeIds
	val ROSERADE: (PkmnType, PkmnType) = ("Grass", "Poison").typeIds
	val SABLEYE: (PkmnType, PkmnType) = ("Dark", "Ghost").typeIds
	val SCIZOR: (PkmnType, PkmnType) = ("Bug", "Steel").typeIds
	val SHEDINJA: (PkmnType, PkmnType) = ("Bug", "Ghost").typeIds
	val SNORLAX: (PkmnType, PkmnType) = ("Normal", "Normal").typeIds
	val SPIRITOMB: (PkmnType, PkmnType) = ("Ghost", "Dark").typeIds
	val TENTACOOL: (PkmnType, PkmnType) = ("Water", "Poison").typeIds
	val TOGEKISS: (PkmnType, PkmnType) = ("Fairy", "Flying").typeIds
	val TORNADUS: (PkmnType, PkmnType) = ("Flying", "Flying").typeIds
	val VAPOREON: (PkmnType, PkmnType) = ("Water", "Water").typeIds
	val VENUSAUR: (PkmnType, PkmnType) = ("Grass", "Poison").typeIds
	val ZAPDOS: (PkmnType, PkmnType) = ("Electric", "Flying").typeIds
}
