/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package types

import Definitions._

class TypeCombinationParserTest extends TestBase {
	"ParserResult" should {
		"replace information" in {
			val uut = ParserResult("name", ("Fire", "Ice").typeIds, "ability", List("Fire", "Ice", "Dark").map(_.typeId))

			(uut.name = "other") should be (ParserResult("other", ("Fire", "Ice").typeIds, "ability", List("Fire", "Ice", "Dark").map(_.typeId)))
			(uut.comb = ("Water", "Grass").typeIds) should be (ParserResult("name", ("Water", "Grass").typeIds, "ability", List("Fire", "Ice", "Dark").map(_.typeId)))
			(uut.ability = "other") should be (ParserResult("name", ("Fire", "Ice").typeIds, "other", List("Fire", "Ice", "Dark").map(_.typeId)))
			(uut.attacks = List("Water", "Grass").map(_.typeId)) should be (ParserResult("name", ("Fire", "Ice").typeIds, "ability", List("Water", "Grass").map(_.typeId)))
		}

		"return battler" in {
			val matrix = TypeMatrices.gen6

			val uut = ParserResult("name", ("Fire", "Ice").typeIds, "Thick Fat", List("Fire", "Ice", "Dark").map(_.typeId))

			uut.asBattler(matrix) should be ((((9, 13), List(0, 0, 0, 1, 1, 0, -1, 2, -1, -1, 1, 0, 0, -3, 0, 0, 0, -1)), List(9, 13, 16)))
		}

		"return named battler" in {
			val matrix = TypeMatrices.gen6

			val uut = ParserResult("name", ("Fire", "Ice").typeIds, "Thick Fat", List("Fire", "Ice", "Dark").map(_.typeId))

			uut.asNamedBattler(matrix) should be (("name", (((9, 13), List(0, 0, 0, 1, 1, 0, -1, 2, -1, -1, 1, 0, 0, -3, 0, 0, 0, -1)), List(9, 13, 16))))
		}

		"use STAB attacks" in {
			val uut = ParserResult("name", ("Fire", "Ice").typeIds, "ability", Nil)

			uut.withStab() should be (ParserResult("name", ("Fire", "Ice").typeIds, "ability", List("Fire", "Ice").map(_.typeId)))
		}

		"add STAB attacks" in {
			val uut = ParserResult("name", ("Fire", "Ice").typeIds, "ability", List("Dark", "Ice").map(_.typeId))

			uut.withStab() should be (ParserResult("name", ("Fire", "Ice").typeIds, "ability", List("Fire", "Ice", "Dark").map(_.typeId)))
		}
	}

	"TypeCombinationParser" should {
		"parse ability" in {
			val input = "Wonder Guard"
			val expected = Some("Wonder Guard")

			val uut = TypeCombinationParser

			uut.parseAbility(input) should be (expected)
		}

		"fail to parse ability" in {
			val input = "INVALID"

			val uut = TypeCombinationParser

			uut.parseAbility(input) should be (None)
		}

		"parse mono type combination" in {
			val input = "Normal"
			val expected = Some(("Normal", "Normal").typeIds)

			val uut = TypeCombinationParser

			uut.parseCombination(input) should be (expected)
		}

		"parse dual type combination" in {
			val input = "Psychic,Dark"
			val expected = Some(("Psychic", "Dark").typeIds)

			val uut = TypeCombinationParser

			uut.parseCombination(input) should be (expected)
		}

		"fail to parse triple type combination" in {
			val input = "Fire,Grass,Water"

			val uut = TypeCombinationParser

			uut.parseCombination(input) should be (None)
		}

		"fail to parse type combination" in {
			val input = "INVALID"

			val uut = TypeCombinationParser

			uut.parseCombination(input) should be (None)
		}

		"parse attacks" in {
			val input = "Fire,Grass,Water"
			val expected = Some(List("Fire", "Grass", "Water").map(_.typeId))

			val uut = TypeCombinationParser

			uut.parseAttacks(input) should be (expected)
		}

		"fail to parse attacks" in {
			val input = "INVALID"

			val uut = TypeCombinationParser

			uut.parseAttacks(input) should be (None)
		}

		"parse line" in {
			val input = "name;Normal;Wonder Guard;Fire,Grass,Water;xxx"
			val expected = Some(ParserResult("name", ("Normal", "Normal").typeIds, "Wonder Guard", List("Fire", "Grass", "Water").map(_.typeId)))

			val uut = TypeCombinationParser

			uut.parseLine(input) should be (expected)
		}

		"parse line despite invalid attacks if disabled" in {
			val input = "name;Normal;Wonder Guard;INVALID;xxx"
			val params = ParserParameters(parseAttacks = false)
			val expected = Some(ParserResult("name", ("Normal", "Normal").typeIds, "Wonder Guard", Nil))

			val uut = TypeCombinationParser

			uut.parseLine(input, params) should be (expected)
		}

		"parse line despite invalid ability if disabled" in {
			val input = "name;Normal;INVALID;Fire,Grass,Water;xxx"
			val params = ParserParameters(parseAbility = false)
			val expected = Some(ParserResult("name", ("Normal", "Normal").typeIds, "", List("Fire", "Grass", "Water").map(_.typeId)))

			val uut = TypeCombinationParser

			uut.parseLine(input, params) should be (expected)
		}

		"parse line despite invalid segments if disabled" in {
			val input = "name;Normal;INVALID;INVALID;xxx"
			val params = ParserParameters(parseAbility = false, parseAttacks = false)
			val expected = Some(ParserResult("name", ("Normal", "Normal").typeIds, "", Nil))

			val uut = TypeCombinationParser

			uut.parseLine(input, params) should be (expected)
		}

		"fail to parse line with invalid type combination" in {
			val input = "name;INVALID;Wonder Guard;Fire,Grass,Water;xxx"

			val uut = TypeCombinationParser

			uut.parseLine(input) should be (None)
		}
	}
}
