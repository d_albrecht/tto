/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package types

import Definitions._

class TypeMatrixProperties extends TestBase {
	"Generation 1" should {
		"find no seven types that cover all type combinations effectively (where applicable)" in {
			val matrix = TypeMatrices.gen1
			val combinations = matrix.allTypes.combinations(7)
			val typeCombinations = matrix.allUniqueCombinations.filterNot(_.effectiveness.noWeaknesses)

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) > 0)).toList should be (Nil)
		}

		"find eight types that cover all type combinations effectively (where applicable)" in {
			val matrix = TypeMatrices.gen1
			val combinations = matrix.allTypes.combinations(8)
			val typeCombinations = matrix.allUniqueCombinations.filterNot(_.effectiveness.noWeaknesses)
			val expected = Set(
				Set("Flying", "Fighting", "Ground", "Ghost", "Bug", "Rock", "Grass", "Dragon")
			).map(_.map(_.typeId))

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) > 0)).map(_.toSet).toSet should be (expected)
		}

		"find single type that covers all type combinations" in {
			val matrix = TypeMatrices.gen1
			val combinations = matrix.allTypes.combinations(1)
			val typeCombinations = matrix.allUniqueCombinations
			val expected = Set(
				Set("Dragon")
			).map(_.map(_.typeId))

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).map(_.toSet).toSet should be (expected)
		}

		"find two types that cover all type combinations" in {
			val matrix = TypeMatrices.gen1
			val combinations = matrix.allTypes.combinations(2)
			val typeCombinations = matrix.allUniqueCombinations

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).size should be (15)
		}

		"find single type that does some damage to all types that covers all type combinations" in {
			val matrix = TypeMatrices.gen1
			val combinations = matrix.allTypes.filter(t => matrix.allMonoTypes.forall(m => m.effectiveness.evaluateAttack(t) > -5)).combinations(1)
			val typeCombinations = matrix.allUniqueCombinations
			val expected = Set(
				Set("Dragon")
			).map(_.map(_.typeId))

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).map(_.toSet).toSet should be (expected)
		}

		"find three types that do some damage to all types that cover all type combinations" in {
			val matrix = TypeMatrices.gen1
			val combinations = matrix.allTypes.filter(t => matrix.allMonoTypes.forall(m => m.effectiveness.evaluateAttack(t) > -5)).combinations(2)
			val typeCombinations = matrix.allUniqueCombinations

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).size should be (10)
		}

		"detect that all types that do some damage to all types can't cover all type combinations effectively (where applicable)" in {
			val matrix = TypeMatrices.gen1
			val combinations = List(matrix.allTypes.filter(t => matrix.allMonoTypes.forall(m => m.effectiveness.evaluateAttack(t) > -5)))
			val typeCombinations = matrix.allUniqueCombinations.filterNot(_.effectiveness.noWeaknesses)

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) > 0)) should be (Nil)
		}
	}

	"Generation 2 to 5" should {
		"find no eight types that cover all type combinations effectively (where applicable)" in {
			val matrix = TypeMatrices.gen2
			val combinations = matrix.allTypes.combinations(8)
			val typeCombinations = matrix.allUniqueCombinations.filterNot(_.effectiveness.noWeaknesses)

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) > 0)).toList should be (Nil)
		}

		"find nine types that cover all type combinations effectively (where applicable)" in {
			val matrix = TypeMatrices.gen2
			val combinations = matrix.allTypes.combinations(9)
			val typeCombinations = matrix.allUniqueCombinations.filterNot(_.effectiveness.noWeaknesses)
			val expected = Set(
				Set("Flying", "Fighting", "Ground", "Bug", "Rock", "Grass", "Fire", "Dragon", "Dark")
			).map(_.map(_.typeId))

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) > 0)).map(_.toSet).toSet should be (expected)
		}

		"find no single type that covers all type combinations" in {
			val matrix = TypeMatrices.gen2
			val combinations = matrix.allTypes.combinations(1)
			val typeCombinations = matrix.allUniqueCombinations

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).toList should be (Nil)
		}

		"find two types that cover all type combinations" in {
			val matrix = TypeMatrices.gen2
			val combinations = matrix.allTypes.combinations(2)
			val typeCombinations = matrix.allUniqueCombinations
			val expected = Set(
				Set("Fire", "Dragon")
			).map(_.map(_.typeId))

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).map(_.toSet).toSet should be (expected)
		}

		"find no single type that does some damage to all types that covers all type combinations" in {
			val matrix = TypeMatrices.gen2
			val combinations = matrix.allTypes.filter(t => matrix.allMonoTypes.forall(m => m.effectiveness.evaluateAttack(t) > -5)).combinations(1)
			val typeCombinations = matrix.allUniqueCombinations

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).toList should be (Nil)
		}

		"find two types that do some damage to all types that cover all type combinations" in {
			val matrix = TypeMatrices.gen2
			val combinations = matrix.allTypes.filter(t => matrix.allMonoTypes.forall(m => m.effectiveness.evaluateAttack(t) > -5)).combinations(2)
			val typeCombinations = matrix.allUniqueCombinations
			val expected = Set(
				Set("Fire", "Dragon")
			).map(_.map(_.typeId))

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).map(_.toSet).toSet should be (expected)
		}

		"detect that all types that do some damage to all types can't cover all type combinations effectively (where applicable)" in {
			val matrix = TypeMatrices.gen2
			val combinations = List(matrix.allTypes.filter(t => matrix.allMonoTypes.forall(m => m.effectiveness.evaluateAttack(t) > -5)))
			val typeCombinations = matrix.allUniqueCombinations.filterNot(_.effectiveness.noWeaknesses)

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) > 0)) should be (Nil)
		}
	}

	"Generation 6 onwards" should {
		"find no eight types that cover all type combinations effectively" in {
			val matrix = TypeMatrices.gen6
			val combinations = matrix.allTypes.combinations(8)
			val typeCombinations = matrix.allUniqueCombinations

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) > 0)).toList should be (Nil)
		}

		"find nine types that cover all type combinations effectively" in {
			val matrix = TypeMatrices.gen6
			val combinations = matrix.allTypes.combinations(9)
			val typeCombinations = matrix.allUniqueCombinations
			val expected = Set(
				Set("Fighting", "Ground", "Rock", "Grass", "Fire", "Dark", "Fairy", "Poison", "Ghost"),
				Set("Fighting", "Ground", "Rock", "Grass", "Fire", "Dark", "Fairy", "Poison", "Steel"),
				Set("Fighting", "Ground", "Rock", "Grass", "Fire", "Dark", "Fairy", "Flying", "Steel"),
				Set("Fighting", "Ground", "Rock", "Grass", "Fire", "Dark", "Fairy", "Bug", "Steel")
			).map(_.map(_.typeId))

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) > 0)).map(_.toSet).toSet should be (expected)
		}

		"find no two types that cover all type combinations" in {
			val matrix = TypeMatrices.gen6
			val combinations = matrix.allTypes.combinations(2)
			val typeCombinations = matrix.allUniqueCombinations

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).toList should be (Nil)
		}

		"find three types that cover all type combinations" in {
			val matrix = TypeMatrices.gen6
			val combinations = matrix.allTypes.combinations(3)
			val typeCombinations = matrix.allUniqueCombinations

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).size should be (97)
		}

		"find no two types that do some damage to all types that cover all type combinations" in {
			val matrix = TypeMatrices.gen6
			val combinations = matrix.allTypes.filter(t => matrix.allMonoTypes.forall(m => m.effectiveness.evaluateAttack(t) > -5)).combinations(2)
			val typeCombinations = matrix.allUniqueCombinations

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).toList should be (Nil)
		}

		"find three types that do some damage to all types that cover all type combinations" in {
			val matrix = TypeMatrices.gen6
			val combinations = matrix.allTypes.filter(t => matrix.allMonoTypes.forall(m => m.effectiveness.evaluateAttack(t) > -5)).combinations(3)
			val typeCombinations = matrix.allUniqueCombinations
			val expected = Set(
				Set("Flying", "Water", "Dark"),
				Set("Rock", "Fire", "Fairy"),
				Set("Grass", "Water", "Ice"),
				Set("Water", "Dark", "Fairy"),
				Set("Steel", "Dark", "Fairy")
			).map(_.map(_.typeId))

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) >= 0)).map(_.toSet).toSet should be (expected)
		}

		"detect that all types that do some damage to all types can't cover all type combinations effectively" in {
			val matrix = TypeMatrices.gen6
			val combinations = List(matrix.allTypes.filter(t => matrix.allMonoTypes.forall(m => m.effectiveness.evaluateAttack(t) > -5)))
			val typeCombinations = matrix.allUniqueCombinations

			combinations.filter(c => typeCombinations.forall(tc => tc.effectiveness.evaluateAttack(c: _*) > 0)) should be (Nil)
		}
	}
}
