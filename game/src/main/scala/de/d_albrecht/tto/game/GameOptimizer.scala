/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package game

import Definitions._

import policies._

object GameOptimizer {
	def run(matrix: TypeMatrix, memberPolicy: MemberPolicy, teamPolicy: TeamPolicy): List[List[Battler]] = {
		val opponents = matrix.allUniqueCombinations.map(_.asBattler)
		val candidates = matrix.allUniqueCombinations.map(_.asBattler).filter(c => memberPolicy(opponents, matrix)(c) == PolicyResponse.Acceptable).zipWithIndex.map(_.swap).toMap
		loop(candidates, teamPolicy(opponents, matrix)).map(t => t.map(candidates))
	}

	def runWithStartingPoint(matrix: TypeMatrix, memberPolicy: MemberPolicy, teamPolicy: TeamPolicy, startingPoint: StartingPointGenerator): List[List[Battler]] = {
		val opponents = matrix.allUniqueCombinations.map(_.asBattler)
		val candidates = matrix.allUniqueCombinations.map(_.asBattler).filter(c => memberPolicy(opponents, matrix)(c) == PolicyResponse.Acceptable).zipWithIndex.map(_.swap).toMap
		val startingPoints = startingPoint.generate(candidates, opponents, teamPolicy, matrix)
		if (startingPoints.isEmpty) {
			Nil
		} else {
			val startsByLength = startingPoints.groupBy(_.size)
			loopWithStartingPoint(candidates, teamPolicy(opponents, matrix), startsByLength, startsByLength.keySet.min).map(t => t.map(candidates))
		}
	}

	@scala.annotation.tailrec
	def loop(candidates: Map[Int, Battler], teamPolicy: (List[Battler]) => PolicyResponse, teams: List[List[Int]] = List(Nil)): List[List[Int]] = {
		round(candidates, teamPolicy, teams) match {
			case (acceptables@(_ :: _), _) => acceptables
			case (Nil, Nil) => Nil
			case (Nil, insufficients) => loop(candidates, teamPolicy, insufficients)
		}
	}

	@scala.annotation.tailrec
	def loopWithStartingPoint(candidates: Map[Int, Battler], teamPolicy: (List[Battler]) => PolicyResponse, starts: Map[Int, List[List[Int]]], length: Int, teams: List[List[Int]] = Nil): List[List[Int]] = {
		if (starts.isEmpty) {
			loop(candidates, teamPolicy, teams)
		} else {
			(if (starts.isDefinedAt(length)) {
				val startsByStatus = starts(length).groupBy(t => teamPolicy(t.map(candidates))).withDefault(_ => Nil)
				(startsByStatus(PolicyResponse.Acceptable), startsByStatus(PolicyResponse.Insufficient))
			} else {
				(Nil, Nil)
			}) match {
				case (acceptables@(_ :: _), _) => acceptables
				case (Nil, insufficientStarts) => merge(round(candidates, teamPolicy, teams), round(candidates, teamPolicy, insufficientStarts, true)) match {
					case (acceptables@(_ :: _), _) => {
						val acceptableStarts = starts.getOrElse(length + 1, Nil).filter(t => teamPolicy(t.map(candidates)) == PolicyResponse.Acceptable)
						acceptables ::: acceptableStarts
					}
					case (Nil, insufficients) => loopWithStartingPoint(candidates, teamPolicy, starts - length, length + 1, insufficients)
				}
			}
		}
	}

	def round(candidates: Map[Int, Battler], teamPolicy: (List[Battler]) => PolicyResponse, teams: List[List[Int]], fullExpand: Boolean = false): (List[List[Int]], List[List[Int]]) = {
		if (teams.isEmpty) {
			(Nil, Nil)
		} else {
			println(s"game optimizer: team-size ${teams.head.size}, test-teams ${teams.size}")
			val newTeams = expand(candidates, teams, fullExpand).groupBy(t => teamPolicy(t.map(candidates))).withDefault(_ => Nil)
			(newTeams(PolicyResponse.Acceptable), newTeams(PolicyResponse.Insufficient))
		}
	}

	def merge[T](t1: (List[T], List[T]), t2: (List[T], List[T])): (List[T], List[T]) = {
		(t1._1 ::: t2._1, t1._2 ::: t2._2)
	}

	def expand[T](candidates: Map[Int, T], teams: List[List[Int]], fullExpand: Boolean = false): List[List[Int]] = {
		val sortedKeys = candidates.keys.toList.sorted
		def innerExpand(team: List[Int]): List[List[Int]] = {
			if (team.isEmpty)
				sortedKeys.map(List(_))
			else if (fullExpand)
				sortedKeys.diff(team).map(_ :: team)
			else
				sortedKeys.dropWhile(_ <= team.head).diff(team).map(_ :: team)
		}
		teams.flatMap(innerExpand).distinctBy(_.toSet) //distinct is necessary because when launched with specific starting points the lists aren't guaranteed to be unique anymore
	}
}
