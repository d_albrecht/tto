/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package game.policies

import Definitions._

trait Policy[T] {
	final def apply(opponents: List[Battler], matrix: TypeMatrix): (T) => PolicyResponse = {
		(examinee: T) => judge(examinee, opponents, matrix)
	}
	def judge(examinee: T, opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = PolicyResponse.Acceptable
}

enum PolicyResponse {
	case Acceptable
	case Insufficient
	case Conflicting
}

object PolicyResponse {
	/**
	  * Results in the policy to report an conflicting examinee if the condition is met.
	  * Conflicting means that given that member or team, there is no way to improve the result anymore. This is a dead end.
	  * For MemberPolicies insufficiency doesn't make much sense, so for convention reasons use this method to let MemberPolicies fail.
	  * @param condition The condition that triggers this result
	  * @return Conflicting if the condition is met, Acceptable otherwise
	  */
	def conflicting(condition: Boolean): PolicyResponse = { if (condition) then PolicyResponse.Conflicting else PolicyResponse.Acceptable }
	/**
	  * Results in the policy to report an insufficient examinee if the condition is met.
	  * Insufficient means that given that member or team, there is still a way to (additively) improve the result.
	  * @param condition The condition that triggers this result
	  * @return Insufficient if the condition is met, Acceptable otherwise
	  */
	def insufficient(condition: Boolean): PolicyResponse = { if (condition) then PolicyResponse.Insufficient else PolicyResponse.Acceptable }

	extension (response: PolicyResponse) {
		def &&(other: PolicyResponse): PolicyResponse = { if (response.ordinal < other.ordinal) then other else response }
	}
}
