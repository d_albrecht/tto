/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package game.policies

import Definitions._
import PolicyResponse._

trait StabAdvantage extends TeamPolicy {
	override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
		insufficient(opponents.exists(o => !examinee.exists(m => StabAdvantage.eval(m, o)))) && super.judge(examinee, opponents, matrix)
	}
}

object StabAdvantage extends StartingPointGenerator {
	override def generate(candidates: Map[Int, Battler], opponents: List[Battler], teamPolicy: TeamPolicy, matrix: TypeMatrix): List[List[Int]] = {
		findUniqueWeaknessCounter(candidates, opponents, teamPolicy, matrix, findUniqueOpponentCounters(candidates, opponents, teamPolicy, matrix))
	}

	def eval(m: Battler, o: Battler): Boolean = {
		val mu = m.evaluateMatchup(o)
		if (o.defense.effectiveness.noWeaknesses) {
			// In practice mu.offensive can't be positive if "o" has no weaknesses, but this comparison feels more fitting
			mu.offensive >= 0 && mu.defensive > 0
		} else {
			mu.offensive > 0 && mu.defensive >= 0
		}
	}

	def findUniqueOpponentCounters(candidates: Map[Int, Battler], opponents: List[Battler], teamPolicy: TeamPolicy, matrix: TypeMatrix): List[Int] = {
		/*
		 * We exploit here, that if the policy detects that some opponent can only be faced by one candidate, that then this candidate has to be part of the team.
		 * In this process the generator will ensure that (and otherwise fail if) any opponents can't be countered by this policy.
	   */
		val counterClasses = opponents.map(o => (o, candidates.filter{ case (_, c) => eval(c, o) }.map(_._1).toList)).groupBy(_._2.size)
		if (counterClasses.isDefinedAt(0)) {
			val notCountered = counterClasses(0).map(_._1.defense.readable).mkString(", ")
			throw new IllegalStateException(s"For some opponent(s) there is not a single suitable candidate: ${notCountered}")
		} else {
			counterClasses.getOrElse(1, Nil).flatMap(_._2)
		}
	}

	def findUniqueWeaknessCounter(candidates: Map[Int, Battler], opponents: List[Battler], teamPolicy: TeamPolicy, matrix: TypeMatrix, counters: List[Int]): List[List[Int]] = {
		/*
		 * We exploit here, that if any individual opponent has a weakness, we want to attack it super-effectively.
		 * And if this opponent only has a single weakness, then the team has to feature this type at least once.
		 * So, we collect all unique weaknesses and find combinations of candidates that contain all these unique weaknesses.
		 * It is possible that - given suitable dual types among the candidates - that one team member covers two unique weaknesses.
		 * For opponents without a unique weakness, the generator doesn't provide a suitable matchup in this process but relies on the Optimizer to find one.
	   */
		val necessaryTypes = opponents.filter(_.defense.effectiveness.uniqueWeakness).flatMap(_.defense.effectiveness.getWeaknesses).distinct
		val teamPol = teamPolicy(opponents, matrix)
		@scala.annotation.tailrec
		def constructStartingPoints(necessaryTypes: List[PkmnType], teams: List[List[Int]]): List[List[Int]] = {
			if (necessaryTypes.isEmpty) {
				teams
			} else {
				val newMembers = candidates.filter(_._2.attacks.contains(necessaryTypes.head)).map(_._1).toList
				val newTeams = newMembers.flatMap(m => teams.map(t => (m :: t).distinct)).filter(t => teamPol(t.map(candidates)) != PolicyResponse.Conflicting)
				constructStartingPoints(necessaryTypes.tail, newTeams)
			}
		}
		constructStartingPoints(necessaryTypes, List(counters))
	}
}

/**
  * Special implementation that can handle the gen1 matrix and relaxes the condition on previously not handleable combinations.
  * This implementation is NOT compatible with the STG of the base name as this implementation is supposed to work around an exception that the STG raises.
  */
trait StabAdvantageGen1 extends TeamPolicy {
	override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
		insufficient(opponents.exists(o => !StabAdvantageGen1.isCountered(o, examinee, matrix))) && super.judge(examinee, opponents, matrix)
	}
}

object StabAdvantageGen1 extends StartingPointGenerator {
	override def generate(candidates: Map[Int, Battler], opponents: List[Battler], teamPolicy: TeamPolicy, matrix: TypeMatrix): List[List[Int]] = {
		StabAdvantage.findUniqueWeaknessCounter(candidates, opponents, teamPolicy, matrix, findUniqueOpponentCounters(candidates, opponents, teamPolicy, matrix))
	}

	def isCountered(opponent: Battler, examinee: List[Battler], matrix: TypeMatrix): Boolean = {
		if (opponent.defense.comb == ("Dragon", "Water").typeIds || opponent.defense.comb == ("Water", "Dragon").typeIds) {
			examinee.exists(m => m.evaluateMatchup(opponent).min >= 0)
		} else {
			examinee.exists(m => StabAdvantage.eval(m, opponent))
		}
	}

	def findUniqueOpponentCounters(candidates: Map[Int, Battler], opponents: List[Battler], teamPolicy: TeamPolicy, matrix: TypeMatrix): List[Int] = {
		val counterClasses = opponents.map(o => (o, candidates.filter{ case (_, c) => isCountered(o, List(c), matrix) }.map(_._1).toList)).groupBy(_._2.size)
		if (counterClasses.isDefinedAt(0)) {
			val notCountered = counterClasses(0).map(_._1.defense.readable).mkString(", ")
			throw new IllegalStateException(s"For some opponent(s) there is not a single suitable candidate: ${notCountered}")
		} else {
			counterClasses.getOrElse(1, Nil).flatMap(_._2)
		}
	}
}
