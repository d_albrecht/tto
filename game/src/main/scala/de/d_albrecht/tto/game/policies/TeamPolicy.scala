/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package game.policies

import Definitions._
import PolicyResponse._

trait StartingPointGenerator {
	/**
	  * As exhaustive searches can get quite heavy both on memory and time, you effectively have two ways to assist the search operation:
	  * - use many "pruning policies" (aka those that even early on are able to exclude whole branches via some "conflicting"-criterion), or
	  * - provide some better suited / more focused starting point to the optimizer.
	  * This should serve the latter approach. Especially for policies that don't make use of "conflicting"-clauses,
	  * but that have a rather complex condition and therefore can build up some more or less complex starting points already, this will speed up the optimization.
	  * Hence, if some policy uses "conflicting" or uses a rather basic strategy, these shouldn't bother with this optional way of utilizing their own strategy.
	  * 
	  * If a policy however is able to more or less reverse engineer its own strategy, then implementing this interface is a way to help the optimizer.
	  * But this comes with certain criteria to this additional implementation:
	  * - the provided starting points don't have to be acceptable solutions on their own, they should serve as "starting points" really,
	  * - in fact, the provided starting points don't even have to be acceptable for the policy a generator is based on, the general idea is to "exploit" SOME part(s) of the way a policy works,
	  * - any provided starting point can't conflict with any of the given policies,
	  * - the starting points will be expanded later on by the optimizer, so each solution will be a superset of either of the starting points,
	  * - the provided starting points have to be exhaustive in general, all acceptible solutions that the optimizer could have found given enough time and memory should be represented in the starting point set,
	  * - if a policy provides starting points that aren't exhaustive, then this has to be clearly indicated somehow,
	  * - the starting points don't have to be of identical size, the optimizer can handle differently sized starting points,
	  * - no effort must be spent to prevent duplicates in the optimization process, a policy shouldn't provide the same starting point twice, but it is up to the optimizer to filter out duplicates due to expansion,
	  * - ideally no starting point should be a subset of another.
	  * 
	  * If in this process a generator identifies a situation in which the normal "judge"-funtion would never indicate an acceptable solution, it is urged to fail with a meaningful message.
	  * If a policy offers a starting point generator but in the process can't identify useful starting points then it is free to fail or return an empty result.
	  * If you want to use this feature you have to use the appropriate optimizer method and supply a suitable/matching implementation.
	  * Generally you are discouraged from using a starting point generator of a policy you aren't also using because it isn't clear to which degree this policy then will be executed.
	  */
	def generate(candidates: Map[Int, Battler], opponents: List[Battler], teamPolicy: TeamPolicy, matrix: TypeMatrix): List[List[Int]]
}

trait TeamPolicy extends Policy[List[Battler]]

trait EnsureResistances(count: Int) extends TeamPolicy {
	override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
		insufficient(matrix.allTypes.exists(t => examinee.count(b => {
			val resistances = b.defense.effectiveness.getResistances
			resistances.contains(t)
		}) < count)) && super.judge(examinee, opponents, matrix)
	}
}

trait LimitedWeaknessesAbs(limit: Int) extends TeamPolicy {
	override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
		conflicting(matrix.allTypes.exists(t => examinee.count(b => {
			val weaknesses = b.defense.effectiveness.getWeaknesses
			weaknesses.contains(t)
		}) > limit)) && super.judge(examinee, opponents, matrix)
	}
}

trait LimitedWeaknessesPct(limit: Double) extends TeamPolicy {
	override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
		val lmt = limit * examinee.size
		insufficient(matrix.allTypes.exists(t => examinee.count(b => {
			val weaknesses = b.defense.effectiveness.getWeaknesses
			weaknesses.contains(t)
		}) > lmt)) && super.judge(examinee, opponents, matrix)
	}
}

trait MaximumTeamSize(limit: Int = 6) extends TeamPolicy {
	override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
		conflicting(examinee.size > limit) && super.judge(examinee, opponents, matrix)
	}
}

trait MinimumTeamSize(limit: Int = 6) extends TeamPolicy {
	override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
		insufficient(examinee.size < limit) && super.judge(examinee, opponents, matrix)
	}
}

trait NoDuplicateTypes extends TeamPolicy {
	override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
		val types = examinee.map(_.defense.comb.toList)
		conflicting(matrix.allTypes.exists(t => types.count(_.contains(t)) > 1)) && super.judge(examinee, opponents, matrix)
	}
}

trait TypePresence(types: List[PkmnType]) extends TeamPolicy {
	override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
		insufficient(types.exists(t => !examinee.exists(_.defense.comb.toList.contains(t)))) && super.judge(examinee, opponents, matrix)
	}
}
