/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package game

import Definitions._

import policies._
import types._

import org.scalatest.Ignore

@Ignore // These tests take several minutes to run them all. But as these tests can be seen as integration tests, it isn't possible to speed them up.
class GameOptimizerApplicationTest extends TestBase {
	"Applying the Game Optimizer" should {
		"not find a perfect team in gen1" in {
			/** This fails for at least the following reasons:
			  * - PerfectMatchup on gen1 can't find a matchup for Kingdra-clones
			  * - the Dragon-type isn't resisted by any type in gen1
			  * This is a known shortcoming of this policy. But this only happens for the insufficiently designed type matrix of gen 1
			  * and will - with high confidence - not be a problem going forward. And that this can happen for gen 1 isn't a major problem
			  * as the Dragon-type in gen 1 was never meant to be actually used in regards to the type matrix.
			  */
			val matrix = TypeMatrices.gen1
			val memPol = new MemberPolicy with NoFourTimesWeakness with NoSelfWeakness with OnlyDualType {}
			val teamPol = new TeamPolicy with StabAdvantage with LimitedWeaknessesAbs(3) with NoDuplicateTypes with EnsureResistances(1) {}

			val uut = GameOptimizer

			uut.run(matrix, memPol, teamPol) should be (Nil)
		}

		"find an almost perfect team in gen1" in {
			val matrix = TypeMatrices.gen1
			val memPol = new MemberPolicy with NoFourTimesWeakness with OnlyDualType {}
			val teamPol = new TeamPolicy with StabAdvantageGen1 with LimitedWeaknessesAbs(2) with MaximumTeamSize(7) {}

			val expected = Set(
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Ghost"), ("Rock", "Grass"), ("Water", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Ghost"), ("Grass", "Water"), ("Rock", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Electric"), ("Normal", "Grass"), ("Water", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Electric"), ("Ghost", "Grass"), ("Water", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Electric"), ("Rock", "Grass"), ("Water", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Electric"), ("Grass", "Water"), ("Rock", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Electric"), ("Grass", "Water"), ("Water", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Electric"), ("Grass", "Electric"), ("Water", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Psychic"), ("Rock", "Grass"), ("Water", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Psychic"), ("Grass", "Water"), ("Rock", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Rock"), ("Fighting", "Dragon"), ("Grass", "Water"), ("Water", "Electric")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Electric"), ("Fighting", "Fire"), ("Rock", "Grass"), ("Water", "Dragon")),
				Set(("Ghost", "Psychic"), ("Flying", "Psychic"), ("Ground", "Ice"), ("Bug", "Dragon"), ("Fighting", "Electric"), ("Grass", "Water"), ("Ghost", "Rock")),
				Set(("Ghost", "Psychic"), ("Flying", "Electric"), ("Ground", "Psychic"), ("Bug", "Rock"), ("Fighting", "Ice"), ("Grass", "Water"), ("Water", "Dragon"))
			).map(_.map(_.typeIds))

			val uut = GameOptimizer

			uut.runWithStartingPoint(matrix, memPol, teamPol, StabAdvantageGen1).map(_.map(_.defense.comb).toSet).toSet should be (expected)
		}

		"find a perfect team in gen2-5" in {
			val matrix = TypeMatrices.gen2
			val memPol = new MemberPolicy with NoFourTimesWeakness with NoSelfWeakness with OnlyDualType {}
			val teamPol = new TeamPolicy with StabAdvantage with LimitedWeaknessesAbs(3) with MaximumTeamSize(7) with EnsureResistances(1) {}

			val expected = Set(
				Set(("Rock", "Grass"), ("Fire", "Dark"), ("Fighting", "Water"), ("Flying", "Steel"), ("Ground", "Bug"), ("Psychic", "Ice"), ("Dragon", "Steel"))
			).map(_.map(_.typeIds))

			val uut = GameOptimizer

			uut.runWithStartingPoint(matrix, memPol, teamPol, StabAdvantage).map(_.map(_.defense.comb).toSet).toSet should be (expected)
		}

		"find a perfect team allowing mono types in gen2-5" in {
			val matrix = TypeMatrices.gen2
			val memPol = new MemberPolicy with NoFourTimesWeakness with NoSelfWeakness {}
			val teamPol = new TeamPolicy with StabAdvantage with LimitedWeaknessesAbs(3) with MaximumTeamSize(7) with EnsureResistances(1) {}

			val expected = Set(
				Set(("Rock", "Grass"), ("Fire", "Dark"), ("Fighting", "Water"), ("Flying", "Steel"), ("Ground", "Bug"), ("Psychic", "Ice"), ("Dragon", "Steel"))
			).map(_.map(_.typeIds))

			val uut = GameOptimizer

			uut.runWithStartingPoint(matrix, memPol, teamPol, StabAdvantage).map(_.map(_.defense.comb).toSet).toSet should be (expected)
		}

		"find a perfect team in gen6+" in {
			val matrix = TypeMatrices.gen6
			val memPol = new MemberPolicy with NoFourTimesWeakness with NoSelfWeakness with OnlyDualType {}
			val teamPol = new TeamPolicy with StabAdvantage with LimitedWeaknessesAbs(2) with MaximumTeamSize(6) with EnsureResistances(1) with NoDuplicateTypes {}

			val expected = Set(
				Set(("Rock", "Grass"), ("Fire", "Dark"), ("Water", "Fairy"), ("Flying", "Steel"), ("Ground", "Bug"), ("Fighting", "Electric")),
				Set(("Rock", "Grass"), ("Fire", "Dark"), ("Water", "Fairy"), ("Flying", "Steel"), ("Ground", "Bug"), ("Fighting", "Psychic"))
			).map(_.map(_.typeIds))

			val uut = GameOptimizer

			uut.runWithStartingPoint(matrix, memPol, teamPol, StabAdvantage).map(_.map(_.defense.comb).toSet).toSet should be (expected)
		}

		"find a perfect team allowing mono types in gen6+" in {
			val matrix = TypeMatrices.gen6
			val memPol = new MemberPolicy with NoFourTimesWeakness with NoSelfWeakness {}
			val teamPol = new TeamPolicy with StabAdvantage with LimitedWeaknessesAbs(2) with MaximumTeamSize(6) with EnsureResistances(1) with NoDuplicateTypes {}

			val expected = Set(
				Set(("Rock", "Grass"), ("Fire", "Dark"), ("Water", "Fairy"), ("Flying", "Steel"), ("Ground", "Bug"), ("Fighting", "Electric")),
				Set(("Rock", "Grass"), ("Fire", "Dark"), ("Water", "Fairy"), ("Flying", "Steel"), ("Ground", "Bug"), ("Fighting", "Psychic")),
				Set(("Rock", "Grass"), ("Fire", "Dark"), ("Water", "Fairy"), ("Flying", "Steel"), ("Ground", "Ground"), ("Fighting", "Electric")),
				Set(("Rock", "Grass"), ("Fire", "Dark"), ("Water", "Fairy"), ("Flying", "Steel"), ("Ground", "Ground"), ("Fighting", "Psychic")),
				Set(("Rock", "Grass"), ("Fighting", "Fire"), ("Water", "Fairy"), ("Flying", "Steel"), ("Ground", "Ground"), ("Electric", "Dark"))
			).map(_.map(_.typeIds))

			val uut = GameOptimizer

			uut.runWithStartingPoint(matrix, memPol, teamPol, StabAdvantage).map(_.map(_.defense.comb).toSet).toSet should be (expected)
		}

		"demonstrate conflicting auxiliary policies in gen6+" in {
			val matrix = TypeMatrices.gen6
			val memPol = new MemberPolicy with NoFourTimesWeakness with NoSelfWeakness with OnlyDualType {}
			val teamPol = new TeamPolicy with LimitedWeaknessesAbs(2) with EnsureResistances(2) with NoDuplicateTypes {}

			val uut = GameOptimizer

			uut.run(matrix, memPol, teamPol) should be (Nil)
		}
	}
}
