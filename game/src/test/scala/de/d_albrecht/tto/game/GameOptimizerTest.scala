/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package game

import Definitions._
import policies.PolicyResponse._

import policies._
import types._

import java.io.ByteArrayOutputStream

import DefinitionsFixture._

class GameOptimizerTest extends TestBase {
	"GameOptimizer" should {
		"run" in {
			val matrix = List(List(0, 0, 0, 0), List(0, 0, 0, 0), List(0, 0, 0, 0), List(0, 0, 0, 0)).asMatrix()
			val memPol = new MemberPolicy {
				override def judge(examinee: Battler, opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
					conflicting(examinee.attacks.size == 2 && examinee.attacks.map(_.asInt % 2).distinct.size == 1) &&
					conflicting(examinee.attacks.size == 1 && examinee.attacks.exists(_.asInt % 2 != 0)) && super.judge(examinee, opponents, matrix)
				}
			}
			val teamPol = new TeamPolicy {
				override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
					conflicting(examinee.flatMap(_.defense.comb.toList).distinct.size == 4) &&
					insufficient(examinee.size < 3 || examinee.flatMap(_.defense.comb.toList).distinct.size < 3)
				}
			}
			val expected = Set(
				Set((0, 0), (2, 2), (0, 1)), Set((0, 0), (2, 2), (0, 3)), Set((0, 0), (0, 1), (0, 3)), Set((0, 0), (0, 1), (1, 2)), Set((0, 0), (0, 3), (2, 3)),
				Set((0, 0), (2, 2), (1, 2)), Set((0, 0), (2, 2), (2, 3)), Set((2, 2), (1, 2), (2, 3)), Set((2, 2), (2, 3), (0, 3)), Set((2, 2), (1, 2), (0, 1))
			)

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.run(matrix, memPol, teamPol).map(_.map(_.defense.comb).toSet).toSet should be (expected)
			}
			out.toString() should be ("game optimizer: team-size 0, test-teams 1\ngame optimizer: team-size 1, test-teams 6\ngame optimizer: team-size 2, test-teams 13\n")
		}

		"run with starting points" in {
			val matrix = List(List(0, 0, 0, 0), List(0, 0, 0, 0), List(0, 0, 0, 0), List(0, 0, 0, 0)).asMatrix()
			val memPol = new MemberPolicy {
				override def judge(examinee: Battler, opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
					conflicting(examinee.attacks.size == 2 && examinee.attacks.map(_.asInt % 2).distinct.size == 1) &&
					conflicting(examinee.attacks.size == 1 && examinee.attacks.exists(_.asInt % 2 != 0)) && super.judge(examinee, opponents, matrix)
				}
			}
			val teamPol = new TeamPolicy {
				override def judge(examinee: List[Battler], opponents: List[Battler], matrix: TypeMatrix): PolicyResponse = {
					conflicting(examinee.flatMap(_.defense.comb.toList).distinct.size == 4) &&
					insufficient(examinee.size < 3 || examinee.flatMap(_.defense.comb.toList).distinct.size < 3)
				}
			}
			val spg = new StartingPointGenerator {
				override def generate(candidates: Map[Int, Battler], opponents: List[Battler], teamPolicy: TeamPolicy, matrix: TypeMatrix): List[List[Int]] = {
					// This implementation breaks the SPG contract to test a specific aspect of the GameOptimizer
					List(List(asCombination((0, 0)), asCombination((2, 2))).flatMap(t => candidates.filter(_._2.defense.comb == t).toList.map(_._1)))
				}
			}

			val expected = Set(
				Set((0, 0), (2, 2), (0, 1)), Set((0, 0), (2, 2), (0, 3)), Set((0, 0), (2, 2), (1, 2)), Set((0, 0), (2, 2), (2, 3))
			)

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.runWithStartingPoint(matrix, memPol, teamPol, spg).map(_.map(_.defense.comb).toSet).toSet should be (expected)
			}
			out.toString() should be ("game optimizer: team-size 2, test-teams 1\n")

		}

		"run with no starting points" in {
			val matrix = List(List(0)).asMatrix()
			val spg = new StartingPointGenerator {
				override def generate(candidates: Map[Int, Battler], opponents: List[Battler], teamPolicy: TeamPolicy, matrix: TypeMatrix): List[List[Int]] = Nil
			}

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.runWithStartingPoint(matrix, new MemberPolicy {}, new TeamPolicy {}, spg) should be (Nil)
			}

			out.toString() should be ("")
		}

		"loop with no teams" in {
			val candidates = (0 to 8).toList.map(n => n -> asTestBattler((n % 3, n / 3), List(0, 0, 0), List(n % 3, n / 3))).toMap
			val pol = (t: List[Battler]) => PolicyResponse.Acceptable
			val teams = Nil

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.loop(candidates, pol, teams) should be (Nil)
			}
			out.toString() should be ("")
		}

		"loop with only conflicting teams" in {
			val candidates = (0 to 8).toList.map(n => n -> asTestBattler((n % 3, n / 3), List(0, 0, 0), List(n % 3, n / 3))).toMap
			val pol = (t: List[Battler]) => if (t.size == 2) PolicyResponse.Conflicting else PolicyResponse.Insufficient

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.loop(candidates, pol) should be (Nil)
			}
			out.toString() should be ("game optimizer: team-size 0, test-teams 1\ngame optimizer: team-size 1, test-teams 9\n")
		}

		"loop" in {
			val candidates = (0 to 8).toList.map(n => n -> asTestBattler((n % 3, n / 3), List(0, 0, 0), List(n % 3, n / 3))).toMap
			val pol = (t: List[Battler]) => {
				if (t.map(_.defense.comb._2).distinct.size != t.size)
					PolicyResponse.Conflicting
				else if (t.map(_.defense.comb._1).distinct.size == 3)
					PolicyResponse.Acceptable
				else
					PolicyResponse.Insufficient
			}
			val teams = List(List(1))
			val expected = List(List(8, 3, 1), List(6, 5, 1))

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.loop(candidates, pol, teams) should be (expected)
			}
			out.toString() should be ("game optimizer: team-size 1, test-teams 1\ngame optimizer: team-size 2, test-teams 6\n")
		}

		"loop with starting points though insufficient teams become acceptible" in {
			val candidates = (0 to 8).toList.map(n => n -> asTestBattler((n % 3, n / 3), List(0, 0, 0), List(n % 3, n / 3))).toMap
			val pol = (t: List[Battler]) => {
				if (t.map(_.defense.comb._2).distinct.size != t.size)
					PolicyResponse.Conflicting
				else if (t.map(_.defense.comb._1).distinct.size == 3)
					PolicyResponse.Acceptable
				else
					PolicyResponse.Insufficient
			}
			val teams = List(List(1))
			val starts = Map(2 -> List(List(2, 4), List(2, 5), List(2, 6)), 3 -> List(List(2, 3, 8), List(2, 7, 8), List(0, 4, 8)))
			val expected = List(List(8, 3, 1), List(6, 5, 1), List(6, 2, 4), List(0, 4, 8))

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.loopWithStartingPoint(candidates, pol, starts, 1, teams) should be (expected)
			}
			out.toString() should be ("game optimizer: team-size 1, test-teams 1\ngame optimizer: team-size 2, test-teams 6\ngame optimizer: team-size 2, test-teams 3\n")
		}

		"loop with no starting points" in {
			val candidates = (0 to 8).toList.map(n => n -> asTestBattler((n % 3, n / 3), List(0, 0, 0), List(n % 3, n / 3))).toMap
			val pol = (t: List[Battler]) => {
				if (t.map(_.defense.comb._2).distinct.size != t.size)
					PolicyResponse.Conflicting
				else if (t.map(_.defense.comb._1).distinct.size == 3)
					PolicyResponse.Acceptable
				else
					PolicyResponse.Insufficient
			}
			val teams = List(List(1))
			val expected = List(List(8, 3, 1), List(6, 5, 1))

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.loopWithStartingPoint(candidates, pol, Map(), 1, teams) should be (expected)
			}
			out.toString() should be ("game optimizer: team-size 1, test-teams 1\ngame optimizer: team-size 2, test-teams 6\n")
		}

		"loop with acceptible starting points" in {
			val candidates = (0 to 8).toList.map(n => n -> asTestBattler((n % 3, n / 3), List(0, 0, 0), List(n % 3, n / 3))).toMap
			val pol = (t: List[Battler]) => {
				if (t.map(_.defense.comb._1.asInt).distinct.size == 1)
					PolicyResponse.Acceptable
				else
					PolicyResponse.Insufficient
			}
			val starts = Map(2 -> List(List(1, 2), List(1, 3), List(1, 4), List(2, 4), List(2, 5), List(2, 6)), 3 -> List(List(1, 2, 3), List(1, 4, 7), List(0, 4, 8)))
			val expected = List(List(1, 4), List(2, 5))

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.loopWithStartingPoint(candidates, pol, starts, 2, Nil) should be (expected)
			}
			out.toString() should be ("")
		}

		"loop with conflicting starting points" in {
			val candidates = (0 to 8).toList.map(n => n -> asTestBattler((n % 3, n / 3), List(0, 0, 0), List(n % 3, n / 3))).toMap
			val pol = (t: List[Battler]) => {
				if (t.map(_.defense.comb._2).distinct.size != t.size)
					PolicyResponse.Conflicting
				else if (t.map(_.defense.comb._1).distinct.size == 3)
					PolicyResponse.Acceptable
				else
					PolicyResponse.Insufficient
			}
			val starts = Map(
				2 -> List(List(0, 1), List(1, 2), List(0, 2), List(3, 4), List(4, 5), List(3, 5), List(6, 7), List(7, 8), List(6, 8)),
				3 -> List(List(1, 2, 3), List(4, 5, 6), List(0, 7, 8)),
				4 -> List(List(1, 2, 3, 4))
			)

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.loopWithStartingPoint(candidates, pol, starts, 2, Nil) should be (Nil)
			}
			out.toString() should be ("")
		}

		"loop with insufficient starting points that all eventually conflict" in {
			val candidates = (0 to 8).toList.map(n => n -> asTestBattler((n % 3, n / 3), List(0, 0, 0), List(n % 3, n / 3))).toMap
			val pol = (t: List[Battler]) => {
				if (t.size % 2 == 1)
					PolicyResponse.Insufficient
				else
					PolicyResponse.Conflicting
			}
			val starts = Map(
				1 -> List(List(1), List(3), List(4), List(2), List(5), List(6)),
				3 -> List(List(1, 2, 3), List(1, 4, 7), List(0, 4, 8)),
				5 -> List(List(1, 2, 3, 4, 5), List(1, 3, 4, 6, 7), List(0, 2, 4, 6, 8))
			)

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.loopWithStartingPoint(candidates, pol, starts, 1, Nil) should be (Nil)
			}
			out.toString() should be ("game optimizer: team-size 1, test-teams 6\ngame optimizer: team-size 3, test-teams 3\ngame optimizer: team-size 5, test-teams 3\n")
		}

		"round with no teams" in {
			val candidates = Map[Int, Battler]()
			val pol = (_: List[Battler]) => PolicyResponse.Acceptable
			val teams = Nil

			val uut = GameOptimizer

			uut.round(candidates, pol, teams, true) should be ((Nil, Nil))
			uut.round(candidates, pol, teams, false) should be ((Nil, Nil))
		}

		"round" in {
			val candidates = (0 to 4).toList.map(n => n -> asTestBattler((n, n), List(0, 0, 0, 0, 0), List(n))).toMap
			val pol = (t: List[Battler]) => t.head.defense.comb._1.asInt match {
				case 2 => PolicyResponse.Acceptable
				case 3 => PolicyResponse.Insufficient
				case 4 => PolicyResponse.Conflicting
			}
			val teams = List(List(1))
			val expected = (List(List(2, 1)), List(List(3, 1)))

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.round(candidates, pol, teams) should be (expected)
			}
			out.toString() should be ("game optimizer: team-size 1, test-teams 1\n")
		}

		"round with full expand" in {
			val candidates = (0 to 3).toList.map(n => n -> asTestBattler((n, n), List(0, 0, 0, 0), List(n))).toMap
			val pol = (t: List[Battler]) => t.head.defense.comb._1.asInt match {
				case 0 => PolicyResponse.Acceptable
				case 2 => PolicyResponse.Insufficient
				case 3 => PolicyResponse.Conflicting
			}
			val teams = List(List(1))
			val expected = (List(List(0, 1)), List(List(2, 1)))

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.round(candidates, pol, teams, true) should be (expected)
			}
			out.toString() should be ("game optimizer: team-size 1, test-teams 1\n")
		}

		"round with larger teams" in {
			val candidates = (0 to 8).toList.map(n => n -> asTestBattler((n % 3, n / 3), List(0, 0, 0), List(n % 3, n / 3))).toMap
			val pol = (t: List[Battler]) => {
				if (t.map(_.defense.comb._2).distinct.size != t.size)
					PolicyResponse.Conflicting
				else if (t.map(_.defense.comb._1).distinct.size == 3)
					PolicyResponse.Acceptable
				else
					PolicyResponse.Insufficient
			}
			val teams = List(List(0, 5), List(1, 4), List(2, 6))
			val expected = (List(List(7, 0, 5), List(4, 2, 6)), List(List(6, 0, 5), List(8, 0, 5), List(6, 1, 4), List(7, 1, 4), List(8, 1, 4), List(3, 2, 6), List(5, 2, 6)))

			val uut = GameOptimizer
			val out = new ByteArrayOutputStream()

			Console.withOut(out) {
				uut.round(candidates, pol, teams) should be (expected)
			}
			out.toString() should be ("game optimizer: team-size 2, test-teams 3\n")
		}

		"merge" in {
			val t1 = (List("1", "2", "3"), List("A", "B", "C"))
			val t2 = (List("4", "5", "6"), List("D", "E", "F"))
			val expected = (List("1", "2", "3", "4", "5", "6"), List("A", "B", "C", "D", "E", "F"))

			val uut = GameOptimizer

			uut.merge(t1, t2) should be (expected)
		}

		"expand on empty list" in {
			val candidates = Map(0 -> ())
			val teams = Nil

			val uut = GameOptimizer

			uut.expand(candidates, teams, true) should be (Nil)
			uut.expand(candidates, teams, false) should be (Nil)
		}

		"expand on empty team" in {
			val candidates = Map(0 -> (), 1 -> ())
			val teams = List(Nil)
			val expected = List(List(0), List(1))

			val uut = GameOptimizer

			uut.expand(candidates, teams, true) should be (expected)
			uut.expand(candidates, teams, false) should be (expected)
		}

		"expand on some team" in {
			val candidates = Map(0 -> (), 1 -> (), 2 -> (), 3 -> ())
			val teams = List(List(1), List(2), List(3))
			val expected = List(List(2, 1), List(3, 1), List(3, 2))

			val uut = GameOptimizer

			uut.expand(candidates, teams) should be (expected)
		}

		"expand on some team with full expand" in {
			val candidates = Map(0 -> (), 1 -> (), 2 -> (), 3 -> ())
			val teams = List(List(1), List(2), List(3))
			val expected = List(List(0, 1), List(2, 1), List(3, 1), List(0, 2), List(3, 2), List(0, 3))

			val uut = GameOptimizer

			uut.expand(candidates, teams, true) should be (expected)
		}
	}
}
