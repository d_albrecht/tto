/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package game.policies

import Definitions._

import types._

import TestCombinations._

class MemberPolicyTest extends TestBase {
	"NoFourTimesWeakness" should {
		"accept Bulbasaur in gen 6+" in {
			val matrix = TypeMatrices.gen6
			val candidate = matrix.combination(BULBASAUR).asBattler

			val uut = new NoFourTimesWeakness{}

			uut.judge(candidate, Nil, matrix) should be (PolicyResponse.Acceptable)
		}

		"not accept Bulbasaur in gen 1" in {
			val matrix = TypeMatrices.gen1
			val candidate = matrix.combination(BULBASAUR).asBattler

			val uut = new NoFourTimesWeakness{}

			uut.judge(candidate, Nil, matrix) should be (PolicyResponse.Conflicting)
		}

		"not accept Gyarados" in {
			val matrix = TypeMatrices.gen6
			val candidate = matrix.combination(GYARADOS).asBattler

			val uut = new NoFourTimesWeakness{}

			uut.judge(candidate, Nil, matrix) should be (PolicyResponse.Conflicting)
		}
	}

	"NoSelfWeakness" should {
		"accept Magikarp" in {
			val matrix = TypeMatrices.gen6
			val candidate = matrix.combination(MAGIKARP).asBattler

			val uut = new NoSelfWeakness{}

			uut.judge(candidate, Nil, matrix) should be (PolicyResponse.Acceptable)
		}

		"not accept particular Ghosts" in {
			val matrix = TypeMatrices.gen6
			val candidate = matrix.combination(GASTLY).asBattler

			val uut = new NoSelfWeakness{}

			uut.judge(candidate, Nil, matrix) should be (PolicyResponse.Conflicting)
		}
	}

	"OnlyDualType" should {
		"accept Charizard" in {
			val matrix = TypeMatrices.gen6
			val candidate = matrix.combination(CHARIZARD).asBattler

			val uut = new OnlyDualType{}

			uut.judge(candidate, Nil, matrix) should be (PolicyResponse.Acceptable)
		}

		"not accept Pikachu" in {
			val matrix = TypeMatrices.gen6
			val candidate = matrix.combination(PIKACHU).asBattler

			val uut = new OnlyDualType{}

			uut.judge(candidate, Nil, matrix) should be (PolicyResponse.Conflicting)
		}
	}
}
