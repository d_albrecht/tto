/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package game.policies

import Definitions._

import types._

import TestCombinations._

class StabAdvantageTest extends TestBase {
	"StabAdvantage" should {
		"accept any team against no opponents" in {
			val matrix = TypeMatrices.gen6
			val team = List(matrix.combination(MAGIKARP).asBattler)

			val uut = new StabAdvantage {}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Acceptable)
		}

		"accept dragons against elementals" in {
			val matrix = TypeMatrices.gen6
			val opponents = List(FLAREON, VAPOREON, JOLTEON, LEAFEON).map(matrix.combination(_).asBattler)
			val team = List("Fire", "Grass", "Water", "Ground").map(elem => matrix.combination(("Dragon", elem).typeIds).asBattler)

			val uut = new StabAdvantage {}

			uut.judge(team, opponents, matrix) should be (PolicyResponse.Acceptable)
		}

		"accept neutral damage against no weaknesses" in {
			val matrix = TypeMatrices.gen2
			val opponents = List(SABLEYE).map(matrix.combination(_).asBattler)
			val team = List(PAWNIARD).map(matrix.combination(_).asBattler)

			val uut = new StabAdvantage {}

			uut.judge(team, opponents, matrix) should be (PolicyResponse.Acceptable)
		}

		"not accept neutral damage against no weaknesses if not resistant to opponent" in {
			val matrix = TypeMatrices.gen2
			val opponents = List(SABLEYE).map(matrix.combination(_).asBattler)
			val team = List(GLACEON).map(matrix.combination(_).asBattler)

			val uut = new StabAdvantage {}

			uut.judge(team, opponents, matrix) should be (PolicyResponse.Insufficient)
		}

		"not accept team without full coverage" in {
			val matrix = TypeMatrices.gen6
			val opponents = matrix.allMonoTypes.map(_.asBattler)
			val starters = List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU).map(matrix.combination(_).asBattler)

			val uut = new StabAdvantage {}

			uut.judge(starters, opponents, matrix) should be (PolicyResponse.Insufficient)
		}

		"not be able to find perfect matchups in gen 1" in {
			val matrix = TypeMatrices.gen1
			val allCombinations = matrix.allUniqueCombinations.map(_.asBattler)

			val uut = new StabAdvantage {}

			uut.judge(allCombinations, allCombinations, matrix) should be (PolicyResponse.Insufficient)
		}

		"be able to find perfect matchups in gen 1 when not facing Kingdra-clones" in {
			val matrix = TypeMatrices.gen1
			val allCombinations = matrix.allUniqueCombinations.map(_.asBattler)
			val kingdra = Set(("Dragon", "Water"), ("Water", "Dragon")).map(_.typeIds)
			val opponents = allCombinations.filterNot(comb => kingdra.contains(comb.defense.comb))

			val uut = new StabAdvantage {}

			uut.judge(allCombinations, opponents, matrix) should be (PolicyResponse.Acceptable)
		}

		"be able to find perfect matchups in gen 2-5" in {
			val matrix = TypeMatrices.gen2
			val allCombinations = matrix.allUniqueCombinations.map(_.asBattler)

			val uut = new StabAdvantage {}

			uut.judge(allCombinations, allCombinations, matrix) should be (PolicyResponse.Acceptable)
		}

		"be able to find perfect matchups in gen 6+" in {
			val matrix = TypeMatrices.gen6
			val allCombinations = matrix.allUniqueCombinations.map(_.asBattler)

			val uut = new StabAdvantage {}

			uut.judge(allCombinations, allCombinations, matrix) should be (PolicyResponse.Acceptable)
		}
	}

	"StabAdvantage (gen1)" should {
		"be able to find perfect matchups in gen 1" in {
			val matrix = TypeMatrices.gen1
			val allCombinations = matrix.allUniqueCombinations.map(_.asBattler)

			val uut = new StabAdvantageGen1 {}

			uut.judge(allCombinations, allCombinations, matrix) should be (PolicyResponse.Acceptable)
		}
	}

	"StabAdvantage SPG" should {
		"suggest Fighting for Normal" in {
			val matrix = TypeMatrices.gen6
			val opponents = List(EEVEE).map(matrix.combination(_).asBattler)
			val raw = matrix.allMonoTypes.map(_.asBattler).zipWithIndex
			val candidates = raw.map(_.swap).toMap
			val expected = List(List("Fighting").map(_.typeId).flatMap(t => raw.filter(_._1.defense.comb._1 == t).map(_._2)))

			val uut = StabAdvantage

			uut.generate(candidates, opponents, new TeamPolicy with StabAdvantage {}, matrix) should be (List(List(3)))
		}

		"suggest Ground for Electric" in {
			val matrix = TypeMatrices.gen6
			val opponents = List(JOLTEON).map(matrix.combination(_).asBattler)
			val raw = matrix.allMonoTypes.map(_.asBattler).zipWithIndex
			val candidates = raw.map(_.swap).toMap
			val expected = List(List("Ground").map(_.typeId).flatMap(t => raw.filter(_._1.defense.comb._1 == t).map(_._2)))

			val uut = StabAdvantage

			uut.generate(candidates, opponents, new TeamPolicy with StabAdvantage {}, matrix) should be (expected)
		}

		"suggest teams for all mono types" in {
			val matrix = TypeMatrices.gen6
			val opponents = matrix.allMonoTypes.map(_.asBattler)
			val raw = matrix.allMonoTypes.map(_.asBattler).zipWithIndex
			val candidates = raw.map(_.swap).toMap
			val expected = List(Set("Fighting", "Ground", "Dark").map(_.typeId).flatMap(t => raw.filter(_._1.defense.comb._1 == t).map(_._2)))

			val uut = StabAdvantage

			uut.generate(candidates, opponents, new TeamPolicy with StabAdvantage {}, matrix).map(_.toSet) should be (expected)
		}

		"fail on Kingdra-clones in gen 1" in {
			assertThrows[IllegalStateException] {
				val matrix = TypeMatrices.gen1
				val opponents = List(KINGDRA).map(matrix.combination(_).asBattler)
				val candidates = matrix.allUniqueCombinations.map(_.asBattler).zipWithIndex.map(_.swap).toMap

				val uut = StabAdvantage

				uut.generate(candidates, opponents, new TeamPolicy with StabAdvantage {}, matrix)
			}
		}
	}

	"StabAdvantage SPG (gen1)" should {
		"find some starting points for gen 1" in {
			val matrix = TypeMatrices.gen1
			val opponents = matrix.allUniqueCombinations.map(_.asBattler)
			val memPol = new MemberPolicy with NoFourTimesWeakness with OnlyDualType {}.apply(opponents, matrix)
			// We manually filter out some combinations to reduce the test's run time, but that doesn't change the fact, that this SPG can find solutions at all!
			val candidates = matrix.allUniqueCombinations.filter(comb => (comb.comb._1.asInt % 2) != (comb.comb._2.asInt % 2)).map(_.asBattler).filter(c => memPol(c) == PolicyResponse.Acceptable).zipWithIndex.map(_.swap).toMap

			val uut = StabAdvantageGen1

			uut.generate(candidates, opponents, new TeamPolicy with StabAdvantageGen1 {}, matrix).size should be (134400)
		}

		"demonstrate that even this adapted version can fail with insufficient candidates" in {
			assertThrows[IllegalStateException] {
				val matrix = TypeMatrices.gen1
				val opponents = matrix.allUniqueCombinations.map(_.asBattler)
				val candidates = matrix.allMonoTypes.map(_.asBattler).zipWithIndex.map(_.swap).toMap

				val uut = StabAdvantageGen1

				uut.generate(candidates, opponents, new TeamPolicy with StabAdvantageGen1 {}, matrix)
			}
		}

		"suggest teams for almost all mono types" in {
			val matrix = TypeMatrices.gen1
			val opponents = matrix.allMonoTypes.filter(t => t.comb._1.typeString != "Ghost").map(_.asBattler)
			val raw = matrix.allMonoTypes.map(_.asBattler).zipWithIndex
			val candidates = raw.map(_.swap).toMap
			val expected = List(Set("Bug", "Fighting", "Ground", "Ice").map(_.typeId).flatMap(t => raw.filter(_._1.defense.comb._1 == t).map(_._2)))

			val uut = StabAdvantageGen1

			uut.generate(candidates, opponents, new TeamPolicy with StabAdvantageGen1 {}, matrix).map(_.toSet) should be (expected)
		}
	}
}
