/**
    Team Type Optimizer
    Copyright (C) 2022 - Dennis Albrecht

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package de.d_albrecht.tto
package game.policies

import Definitions._

import types._

import TestCombinations._

class TeamPolicyTest extends TestBase {
	"NoDuplicateTypes" should {
		"exclude team with two Water types" in {
			val matrix = TypeMatrices.gen6
			val team = List(TENTACOOL, KABUTO).map(matrix.combination(_).asBattler)

			val uut = new NoDuplicateTypes{}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Conflicting)
		}

		"accept team with all four final stage gen1 starters" in {
			val matrix = TypeMatrices.gen6
			val team = List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU).map(matrix.combination(_).asBattler)

			val uut = new NoDuplicateTypes{}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Acceptable)
		}
	}

	"MaximumTeamSize" should {
		"exclude an uber Fisher team" in {
			val matrix = TypeMatrices.gen6
			val team = List(MAGIKARP, MAGIKARP, MAGIKARP, MAGIKARP, MAGIKARP, MAGIKARP, MAGIKARP).map(matrix.combination(_).asBattler)

			val uut = new MaximumTeamSize(){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Conflicting)
		}

		"accept team of one" in {
			val matrix = TypeMatrices.gen6
			val team = List(CHARIZARD).map(matrix.combination(_).asBattler)

			val uut = new MaximumTeamSize(2){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Acceptable)
		}
	}

	"MinimumTeamSize" should {
		"not accept an empty team" in {
			val matrix = TypeMatrices.gen6

			val uut = new MinimumTeamSize(1){}

			uut.judge(Nil, Nil, matrix) should be (PolicyResponse.Insufficient)
		}

		"accept Red's team" in {
			val matrix = TypeMatrices.gen6
			val team = List(PIKACHU, ESPEON, SNORLAX, VENUSAUR, CHARIZARD, BLASTOISE).map(matrix.combination(_).asBattler)

			val uut = new MinimumTeamSize(){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Acceptable)
		}
	}

	"LimitedWeaknessesAbs" should {
		"exclude team all weak to Rock" in {
			val matrix = TypeMatrices.gen6
			val team = List(FLAREON, GLACEON, CATERPIE, TORNADUS).map(matrix.combination(_).asBattler)

			val uut = new LimitedWeaknessesAbs(3){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Conflicting)
		}

		"accept team with all four final stage gen1 starters" in {
			val matrix = TypeMatrices.gen6
			val team = List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU).map(matrix.combination(_).asBattler)

			val uut = new LimitedWeaknessesAbs(2){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Acceptable)
		}
	}

	"LimitedWeaknessesPct" should {
		"not accept team all weak to Rock" in {
			val matrix = TypeMatrices.gen6
			val team = List(FLAREON, GLACEON, CATERPIE, TORNADUS).map(matrix.combination(_).asBattler)

			val uut = new LimitedWeaknessesPct(.75){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Insufficient)
		}

		"accept team with all four final stage gen1 starters" in {
			val matrix = TypeMatrices.gen6
			val team = List(VENUSAUR, CHARIZARD, BLASTOISE, RAICHU).map(matrix.combination(_).asBattler)

			val uut = new LimitedWeaknessesPct(.5){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Acceptable)
		}
	}

	"TypePresence" should {
		"not accept Cynthia's team" in {
			val matrix = TypeMatrices.gen6
			val team = List(SPIRITOMB, ROSERADE, GASTRODON, LUCARIO, MILOTIC, GARCHOMP).map(matrix.combination(_).asBattler)

			val uut = new TypePresence(List("Fire", "Grass", "Water").map(_.typeId)){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Insufficient)
		}

		"accept Red's team" in {
			val matrix = TypeMatrices.gen6
			val team = List(PIKACHU, ESPEON, SNORLAX, VENUSAUR, CHARIZARD, BLASTOISE).map(matrix.combination(_).asBattler)

			val uut = new TypePresence(List("Fire", "Grass", "Water").map(_.typeId)){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Acceptable)
		}
	}

	"EnsureResistances" should {
		"not accept a gen1 eeveelution team" in {
			val matrix = TypeMatrices.gen1
			val team = List(VAPOREON, JOLTEON, FLAREON).map(matrix.combination(_).asBattler)

			val uut = new EnsureResistances(1){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Insufficient)
		}

		"accept team of 2 with clever selection" in {
			val matrix = TypeMatrices.gen2
			val team = List(MAGNEMITE, GYARADOS).map(matrix.combination(_).asBattler)

			val uut = new EnsureResistances(1){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Acceptable)
		}

		"not accept team with stricter condition" in {
			val matrix = TypeMatrices.gen2
			val team = List(MAGNEMITE, GYARADOS, SABLEYE).map(matrix.combination(_).asBattler)

			val uut = new EnsureResistances(2){}

			uut.judge(team, Nil, matrix) should be (PolicyResponse.Insufficient)
		}
	}
}
